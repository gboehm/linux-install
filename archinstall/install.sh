#!/bin/sh
set -e

curl -L "https://gitlab.com/gboehm/linux-install/-/raw/master/archinstall/init_config.py" > init_config.py
python init_config.py
chmod 777 ".install_config.json"

curl -L "https://gitlab.com/gboehm/linux-install/-/raw/master/archinstall/interactive.py" > interactive.py
python interactive.py --config "https://gitlab.com/gboehm/linux-install/-/raw/master/archinstall/user_configuration.json" --creds "https://gitlab.com/gboehm/linux-install/-/raw/master/archinstall/user_credentials.json"

cp --preserve=all ".install_config.json" "/mnt/archinstall/home/yoro/"

reboot
