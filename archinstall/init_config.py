#!/usr/bin/env python3

import os.path
import json

def init_config():
    config_file = '.install_config.json'

    config_elements = {
        "pywal": {
            "prompt": "Install pywal?",
            "choices": "y/n",
            "default": "y"
        },
        "tablet": {
            "prompt": "Install drawing tablet support?",
            "choices": "y/n",
            "default": "y"
        },
        "touchpad": {
            "prompt": "Install touchpad support?",
            "choices": "y/n",
            "default": "y"
        },
        "memosync": {
            "prompt": "Install memosync dev environment?",
            "choices": "y/n",
            "default": "y"
        },
        "gaming": {
            "prompt": "Install gaming related stuff?",
            "choices": "y/n",
            "default": "y"
        },
        "vpn": {
            "prompt": "Install a vpn?",
            "choices": "y/n",
            "default": "y"
        },
        "virt": {
            "prompt": "Install some virtualization?",
            "choices": [
                "virtbox",
                "virtman",
                "none"
            ],
            "default": "virtman"
        },
        "display": {
            "prompt": "Which display server to use?",
            "choices": [
                "wayland",
                "x11",
                "both"
            ],
            "default": "wayland"
        },
    }
    config = {}

    reuse = False
    if os.path.isfile(config_file):
        choice=''
        while choice != 'y' and choice != 'n':
            choice = input("Config found, use previous config ? (Y/n) ").lower()
            if choice == '':
                choice = 'y'

        if choice.lower() == 'y':
            reuse = True;

    if reuse:
        print("Reusing previous config.")
    else:
        # Input the config

        for option in config_elements:
            choices = config_elements[option]["choices"]
            if choices == "y/n":
                choices = ["y", "n"]
            # prompt_choices = f"(default:{config_elements[option]['default']}"
            prompt_choices = f"({config_elements[option]['default'].upper()}"
            for c in choices:
                if c != config_elements[option]['default']:
                    prompt_choices = f"{prompt_choices},{c}"
            prompt_choices = f"{prompt_choices})"

            choice = ''
            while not choice in choices:
                choice = input(config_elements[option]["prompt"] + " " + prompt_choices + " ").lower()
                if choice == '':
                    choice = config_elements[option]["default"].lower()

            if config_elements[option]["choices"] == "y/n":
                config[option] = (choice == "y")
            else:
                config[option] = choice

        # Write the config
        with open(config_file, "w") as write_file:
            json.dump(config, write_file)

if __name__ == "__main__":
    init_config()
