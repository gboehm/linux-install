#!/bin/sh

sleep 10

mkdir -p /home/yoro/repos

cd /home/yoro/repos || return 1
git clone https://gitlab.com/gboehm/linux-install
cd linux-install || return 1

. .config/myscripts/system/xdg_paths.sh
. .config/myscripts/system/home_declutter.sh

# Created in the live install phase
cp ~/.install_config.json .

# The script sometimes just needs to be restarted so let's do it a couple times
# and break out if we finished correctly
i=0
while [ $i -le 3 ]; do
    python init.py && break
    i=$(( i + 1 ))
done

rm ~/setup.sh
