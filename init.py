#!/usr/bin/env python3

import json
import os
import subprocess

from archinstall.init_config import init_config

# To avoid timeout during install
subprocess.run("echo \"$USER ALL=(ALL) NOPASSWD: ALL\" > \"00_$USER\"", shell=True)
subprocess.run("sudo chown root:root \"00_$USER\"", shell=True)
subprocess.run("sudo mv \"00_$USER\" /etc/sudoers.d/", shell=True)

progress = 0;
progress_jump = 0;

def process():
    global progress
    global progress_jump
    progress = progress + 1
    ret = progress >= progress_jump
    if ret:
        with open(install_progress, "wb") as wf:
            wf.write(progress.to_bytes(length=8, signed=False))
    return ret

def run(cmd, prefix="", suffix="", nofail=False, cwd=None):
    if process():
        print("###################### Running CMD ######################")
        print(cmd)
        print("#########################################################")
        subprocess.run(prefix+cmd+suffix+(" || true" if nofail else ""),check=True, shell=True, cwd=cwd)
    else:
        print("---------------------- Skipping CMD ----------------------")
        print(cmd)
        print("----------------------------------------------------------")

# ssh keys are actually not set yet so not very useful but oh well...
def run_git(cmd, nofail=False, cwd=None):
    run(cmd="GIT_SSH_COMMAND=\"ssh -o StrictHostKeyChecking=accept-new\" git "+cmd, nofail=nofail, cwd=cwd)

def run_supacman(cmd, nofail=False, cwd=None):
    run(cmd="sudo pacman --noconfirm --needed --disable-download-timeout "+cmd, nofail=nofail, cwd=cwd)

def run_suparu(cmd, nofail=False, cwd=None):
    run(cmd="paru --noconfirm --needed --gpgflags \"--yes\" --sudo /bin/sudo "+cmd, nofail=nofail, cwd=cwd)

def run_getOutput(cmd, cwd=None):
    return subprocess.run(cmd,check=True, shell=True, capture_output=True, cwd=cwd).stdout

script_dir=os.path.dirname(os.path.abspath(__file__))

config_file = os.path.join(script_dir,'.install_config.json')
install_progress = os.path.join(script_dir,'.install_progress')

config = {}

if os.path.isfile(config_file):
    print("Config found, using previous config")
else:
    init_config()

with open(config_file, "r") as read_file:
    config = json.load(read_file)

if os.path.isfile(install_progress):
    with open(install_progress, "rb") as read_file:
        progress_jump = int.from_bytes(read_file.read())
    print("Installation progress found, fast forwarding to run number", progress_jump+1)

def copies_and_links():
    x11_only_configs=[
        "bspwm"
        "picom"
        "polybar"
        "sxhkd"
    ]
    wayland_only_configs=[
        "hypr"
        "waybar"
        "kanshi"
        "swappy"
    ]
    linked_configs=[
        "bspwm","btop","clight-gui","dunst","fcitx5","gtk-2.0","gtk-3.0","gtk-4.0","hypr","kanshi","kitty","myscripts","neofetch","nvim","paru","picom","polybar","pulse","qt5ct","qt6ct","ranger","rofi","swappy","sxhkd","systemd","taskell","texstudio","tmux","topgrade.d","wal","waybar","zathura","zsh","alacritty.conf","chromium-flags.conf","electron-flags.conf","spotify-flags.conf","clight.conf","matrix.sh","MControlCenter.conf","mimeapps.list","taskell.md","URL_Auto_Redirector_rules.json","yad.conf","X11"
    ]
    exclude_configs=[]

    exclude_configs = exclude_configs + linked_configs
    if config["display"] == "wayland":
        exclude_configs = exclude_configs + x11_only_configs
    elif config["display"] == "X11":
        exclude_configs = exclude_configs + wayland_only_configs

    if not config["pywal"]:
        exclude_configs = exclude_configs + ["pywal"]

    run(cmd="""\
mkdir -p ~/.config\
""")
    # Copy all .config folders except excluded
    for entry in os.scandir(f"{script_dir}/.config/"):
        if not entry.name in exclude_configs:
            run(cmd=f"""\
cp -r "{script_dir}/.config/{entry.name}" ~/.config\
""")
    # Link folders and files
    for entry in linked_configs:
        fromPath = os.path.join(f"{script_dir}/.config", entry)
        toPath = os.path.expanduser(os.path.join("~/.config", entry))
        run(cmd=f"""\
rm -rf "{toPath}"\
""")
        run(cmd=f"""\
ln -sf "{fromPath}" "{toPath}"\
""")

    # Lins fromPath to toPath/fromName
    special_full_links=[
        f"{script_dir}/.bashrc|~",
        f"{script_dir}/.profile|~",
        f"{script_dir}/.xprofile|~",
        f"{script_dir}/.screensaver_lowres.mp4|~/Videos",
        f"{script_dir}/.local/share/shinit|~/.local/share",
        f"{script_dir}/.local/share/clight|~/.local/share",
        f"{script_dir}/.local/share/applications/my_custom_apps|~/.local/share/applications",
        f"{script_dir}/.local/share/xscreenlayout|~/.local/share",
        f"{script_dir}/.local/share/notepad.html|~/.local/share",
    ]
    # Links fromPath/* to toPath/fromNames
    special_partial_links=[
        f"{script_dir}/.ssh|~/.ssh",
        f"{script_dir}/.local/bin|~/.local/bin",
        f"{script_dir}/.local/share/icons|~/.local/share/icons",
        f"{script_dir}/.local/share/nvim|~/.local/share/nvim",
        f"{script_dir}/.local/share/tmux/plugins|~/.local/share/tmux/plugins",
    ]
    special_partial_copies=[
    ]
    special_partial_root_copies=[
        f"{script_dir}/.etc|/etc",
    ]

    # INFO: Full links
    for entry in special_full_links:
        fromPath = os.path.abspath(os.path.expanduser(entry.split("|")[0]))
        toPath = os.path.abspath(os.path.expanduser(entry.split("|")[1]))
        if(os.path.isdir(fromPath)):
            run(cmd=f"""\
rm -rf "{toPath}/{os.path.basename(os.path.normpath(fromPath))}"\
""")
        run(cmd=f"""\
mkdir -p "{toPath}"\
""")
        run(cmd=f"""\
ln -sf "{fromPath}" -t "{toPath}"\
""")
    # INFO: Partial links
    for entry in special_partial_links:
        fromPath = os.path.abspath(os.path.expanduser(entry.split("|")[0]))
        toPath = os.path.abspath(os.path.expanduser(entry.split("|")[1]))
        run(cmd=f"""\
mkdir -p "{toPath}"\
""")
        for entry in os.listdir(fromPath):
            finalPath = os.path.join(toPath,entry)
            run(cmd=f"""\
rm -rf "{finalPath}"\
""")

            run(cmd=f"""\
ln -sf "{os.path.join(fromPath, entry)}" -t "{toPath}"\
""")
    # INFO: Partial copies
    for entry in special_partial_copies:
        fromPath = os.path.abspath(os.path.expanduser(entry.split("|")[0]))
        toPath = os.path.abspath(os.path.expanduser(entry.split("|")[1]))
        run(cmd=f"""\
cp -r "{fromPath}"{"/*" if os.path.isdir(fromPath) else ""} "{toPath}"\
""")
    # INFO: Partial root copies
    for entry in special_partial_root_copies:
        fromPath = os.path.abspath(os.path.expanduser(entry.split("|")[0]))
        toPath = os.path.abspath(os.path.expanduser(entry.split("|")[1]))
        run(cmd=f"""\
sudo cp -r "{fromPath}"{"/*" if os.path.isdir(fromPath) else ""} "{toPath}"\
""")

    # Link this manually (Maybe do another for loop once I have a couple)
    run(cmd="""\
sudo mkdir -p /usr/share/icons/default
""")
    run(cmd="""\
sudo ln -sf ~/.local/share/icons/default/index.theme /usr/share/icons/default/index.theme
""")


    if config["display"] == "wayland":
        run(cmd="""\
unlink ~/.local/share/xscreenlayout\
""", nofail=True) #arandr screen layouts persistence
    elif config["display"] == "x11":
        run(cmd="""\
unlink ~/.config/spotify-flags.conf\
""", nofail=True) #wayland launch flags

    if config["pywal"]:
        run(cmd="""\
ln -sf ~/.cache/wal/colors-rofi-launcher.rasi ~/.config/rofi/colors-launcher.rasi\
""")
        run(cmd="""\
ln -sf ~/.cache/wal/colors-rofi-sysmenu.rasi ~/.config/rofi/colors-sysmenu.rasi\
""")
        run(cmd="""\
ln -sf ~/.cache/wal/colors-rofi-sinkselector.rasi ~/.config/rofi/colors-sinkselector.rasi\
""")
    else:
        run(cmd="""\
ln -sf ~/.config/rofi/default-colors-launcher.rasi ~/.config/rofi/colors-launcher.rasi\
""")
        run(cmd="""\
ln -sf ~/.config/rofi/default-colors-sysmenu.rasi ~/.config/rofi/colors-sysmenu.rasi\
""")
        run(cmd="""\
ln -sf ~/.config/rofi/default-colors-sinkselector.rasi ~/.config/rofi/colors-sinkselector.rasi\
""")

    run(cmd="""\
sudo sed -ie 's/^# deny.*/deny = 0/' /etc/security/faillock.conf\
""") # stupid lock after 3 login attempts





# INFO: --------------------------------- Install
try:

    # Get the current gpu manufacturer
    gpu = run_getOutput(r'lspci -k 2>&1 | grep VGA | grep -o NVIDIA | uniq || lspci -k 2>&1 | grep VGA | grep -o AMD | uniq || echo \"nope\"').decode()

    # pull submodules
    run_git(cmd="""\
submodule update --init --force --recursive\
""", cwd=script_dir)
    # pull lfs files
    run_git(cmd="""\
lfs install\
""", cwd=script_dir)
    run_git(cmd="""\
lfs pull\
""", cwd=script_dir)

    # install paru aur helper
    run(cmd="""\
rm -rf paru/\
""", cwd=os.path.expanduser("~"))
    run_git(cmd="""\
clone https://aur.archlinux.org/paru.git\
""", cwd=os.path.expanduser("~"))
    run(cmd="""\
makepkg -si --noconfirm\
""", cwd=os.path.expanduser("~/paru"))
    run(cmd="""\
rm -rf paru\
""", cwd=os.path.expanduser("~"))

    run_suparu(cmd="""\
-Sc\
""", nofail=True)

    # Copy and link configs
    copies_and_links()

    #
    #### installs
    #

    # install necessary stuff
    run_supacman(cmd="""\
-Syu pacutils inter-font alsa-utils feh imagemagick ttf-fantasque-sans-mono iproute2 iw ffmpeg xorg-server firefox libxcb pcmanfm lxappearance arc-gtk-theme arc-icon-theme htop btop discord alacritty ripgrep tmux curl cmake zsh thunderbird dotnet-runtime fcitx5-im fcitx5-mozc fcitx5-qt fcitx5-gtk fcitx5-hangul dotnet-sdk networkmanager network-manager-applet adapta-gtk-theme breeze-gtk pavucontrol systemd neofetch yad adobe-source-han-sans-jp-fonts rxvt-unicode libffi ntfs-3g qmk playerctl vlc thefuck kitty mdcat drawing ffmpegthumbnailer catdoc pandoc bat mpv bc dunst hidapi krita gimp eza nodejs npm tldr bluez bluez-utils blueman gnome-calculator intel-gpu-tools zathura zathura-pdf-mupdf qbittorrent ncdu mediainfo duf opendoas dash gnome-calendar tidy stylelint ruby chromium portaudio flatpak sxiv reflector net-tools glow kdenlive syncthing keepassxc dex cppcheck mono read-edid pipewire wireplumber pipewire-pulse texlive-binextra biber sddm ranger libreoffice-fresh ninja openvpn sshfs telegram-desktop transmission-remote-gtk solaar piper firejail luarocks\
""")

    if config["display"] != "wayland":
        run_supacman(cmd="""\
-Syu maim xclip sxhkd picom wmctrl unclutter arandr slock xss-lock xorg-xinit xterm pulsemixer\
""")
    elif config["display"] != "X11":
        run_supacman(cmd="""\
-Syu hyprland waybar qt5-wayland qt6-wayland xdg-desktop-portal-hyprland polkit-kde-agent brightnessctl grim slurp swappy kanshi hypridle hyprlock hyprpaper ydotool\
""")
        # Needs a lib packaged in this for wine systray icons using xembed proxy
        run_supacman(cmd="""\
-Syu plasma-workspace
""")
        run_suparu(cmd="""\
-Syu hyprshade python-pystache\
""")

    # Special installs ( Needed before other installs )
    run(cmd="""\
sudo pacman --noconfirm -Rc rust\
""")
    run_suparu(cmd="""\
-Sy rustup\
""")
    run(cmd="""\
rustup default stable\
""")

    # install stuff from aur
    run_suparu(cmd="""\
--chroot -Sy ttf-ubuntu-mono-nerd otf-firamono-nerd ttf-fantasque-nerd ttf-firacode-nerd ttf-inconsolata-nerd ttf-nerd-fonts-symbols-mono ttf-material-design-icons-git spotify spicetify-cli fontpreview qtkeychain gnome-keyring luv-icon-theme-git spotifywm-git visual-studio-code-bin calendar-cli bfg ytfzf topgrade signal-desktop-beta-bin fcitx5-nord onefetch needrestart clight-gui-git acpi_call-dkms redshift-minimal autofs crow-translate xdg-ninja js-beautify trashy-git moar rofi-lbonn-wayland-git zsa-keymapp-bin ledger-live-bin notesnook-bin qflipper simplex-desktop-appimage unrar zoom nordzy-cursors qt5ct qt6ct simple-sddm-theme-git beeper-latest-bin vesktop-bin cryptomator-bin duplicati-beta-bin taskell-bin\
""")
    if config["display"] != "wayland":
        run_suparu(cmd="""\
-Sy polybar bspwm-rounded-corners ponymix simplescreenrecorder\
""")
    elif config["display"] != "X11":
        run_suparu(cmd="""\
-Sy wev clipman wl-clipboard-history-git hyprprop-git nordzy-hyprcursors\
""")

    # neovim installs
    run_supacman(cmd="""\
-Sy neovim github-cli python-pynvim tree-sitter-cli ccls llvm\
""")
    run_suparu(cmd="""\
-Sy cmigemo-git local-lua-debugger-vscode-git navi omnisharp-roslyn ueberzug\
""")

    # default config files/dirs
    run(cmd="""\
mkdir -p ${XDG_STATE_HOME:-$HOME/.local/state}\
""")
    run(cmd="""\
touch ${XDG_STATE_HOME:-$HOME/.local/state}/python_history\
""")
    run(cmd="""\
mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/simplescreenrecorder\
""")
    run(cmd="""\
mkdir -p ${XDG_CACHE_HOME:-$HOME/.cache}/zsh\
""")

    # npm installs
    run(cmd="""\
sudo npm i gramma -g\
""")
    run(cmd="""\
sudo npm i neovim -g\
""")

    # ruby installs
    run(cmd="""\
gem environment\
""")
    run(cmd="""\
gem install neovim\
""")

    # python installs
    run_supacman(cmd="""\
-Sy python-pip python-pipx python-pygments python-pillow python-virtualenv\
""")
    run_suparu(cmd="""\
-Sy python-fuzzyfinder\
""")
    run(cmd="""\
pipx install --include-deps pyedid\
""") # for monitor scripts

    # default virtual environments
    venvspath=run_getOutput(r'printf "${XDG_CONFIG_HOME:-$HOME/.config}/virtualenvs"').decode()
    run(cmd=f"""\
mkdir -p "{venvspath}"\
""")
    run(cmd="""\
python -m venv debugpy\
""", cwd=venvspath)
    run(cmd="""\
./debugpy/bin/python -m pip install debugpy\
""", cwd=venvspath)

    # pywal related installs
    if config["pywal"]:
        run_suparu(cmd="""\
-Sy python-pywal16 walogram-git python-pywalfox\
""")

    # System info installs
    run_supacman(cmd="""\
-Sy inxi dmidecode fwupd lshw\
""")
    run_suparu(cmd="""\
-Sy imhex-bin\
""")

    # Graphics stack installs
    if gpu == "NVIDIA":
        run_supacman(cmd="""\
-Sy nvidia nvidia-settings nvtop ffnvcodec-headers\
""")
        run(cmd="""\
replace="    ModulePath      \\"\\/usr\\/lib\\/nvidia\\/xorg\\"\\\\
ModulePath      \\"\\/usr\\/lib\\/xorg\\/modules\\""
sudo perl -0777 -i -pe "s/(Section \\"Files\\")[\\w\\W]*?(EndSection)/\\1\\n$replace\\n\\2/g" /etc/X11/xorg.conf\
""")
    elif gpu == "AMD":
        run_supacman(cmd="""\
-Sy mesa xf86-video-amdgpu xf86-video-ati mesa-vdpau nvtop openrgb\
""")
        run_suparu(cmd="""\
-Sy mcontrolcenter-cli-git meestic-git\
""")
    else:
        run_supacman(cmd="""\
-Sy mesa xf86-video-intel\
""")

    # virtualization installs
    if config["virt"] != "none":
        run_supacman(cmd="""\
-Sy linux-headers\
""")
        if config["virt"] == "virtman":
            run(cmd="""\
sudo pacinstall --yolo iptables-nft\
""") # to resolve conflict
            run_supacman(cmd="""\
-Sy virt-manager qemu-base qemu-emulators-full dnsmasq spice-vdagent xf86-video-qxl virt-viewer\
""")
            run(cmd="""\
sudo systemctl enable libvirtd\
""")
            run(cmd="""\
sudo usermod -a -G libvirt "$USER"\
""")
        elif config["virt"] == "virtbox":
            run_supacman(cmd="""\
-Sy virtualbox-guest-iso virtualbox-host-modules-arch virtualbox\
""")

    # tablet install
    if config["tablet"]:

            run_suparu(cmd="""\
-Sy opentabletdriver\
""")
            run(cmd="""\
echo "blacklist hid_uclogic" | sudo tee -a /etc/modprobe.d/blacklist.conf\
""")
            run(cmd="""\
sudo rmmod hid_uclogic\
""", nofail=True)
            run(cmd="""\
systemctl --user daemon-reload\
""")
            run(cmd="""\
systemctl --user enable opentabletdriver --now\
""")

    # touchpad install
    if config["touchpad"]:
            run_supacman(cmd="""\
-Rsn xf86-input-synaptics\
""", nofail=True)
            run_supacman(cmd="""\
-Syu xf86-input-libinput\
""")
            run(cmd=f"""\
sudo mkdir -p /etc/X11/xorg.conf.d && sudo ln -sf "{script_dir}/.xorg.conf.d/30-touchpad.conf" /etc/X11/xorg.conf.d/30-touchpad.conf\
""")

    # memosync installs
    if config["memosync"]:
            run_suparu(cmd="""\
-Sy android-studio android-tools\
""")
            run_suparu(cmd="""\
-Sy fvm\
""")
            run_suparu(cmd="""\
-Sy android-sdk-cmdline-tools-latest\
""", nofail=True)

    if config["gaming"]:
        run(cmd="""\
sudo sed -zi 's%#\\[multilib\\]\\n#Include = /etc/pacman.d/mirrorlist%\\[multilib\\]\\nInclude = /etc/pacman.d/mirrorlist%' /etc/pacman.conf\
""")
        run(cmd="""\
sudo pacman -Sy\
""")
        run_supacman(cmd="""\
-Sy python-magic wine\
""")
        run_suparu(cmd="""\
-Sy proton-ge-custom-bin winetricks-git mangohud\
""")
        run_suparu(cmd="""\
-Sy parsec-bin\
""", nofail=True) # Could fail

        run(cmd="""\
flatpak install --noninteractive bottles flatseal\
""", nofail=True) # Could fail

        run_supacman(cmd="""\
-Sy lib32-pipewire giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader cups samba dosbox gamemode gamescope\
""")
        if config["display"] != "wayland":
            run_supacman(cmd="""\
-Sy libxcomposite lib32-libxcomposite libxinerama lib32-libxinerama\
""")
        elif config["display"] != "X11":
            pass

        # Gaming stuff
        run_supacman(cmd="""\
-Sy steam lutris\
""")
        run_suparu(cmd="""\
-Sy basemark heroic-games-launcher-bin lug-helper prismlauncher-qt5-bin sidequest-bin\
""")
        if gpu == "NVIDIA":
            run_supacman(cmd="""\
-Sy nvidia-dkms nvidia-utils lib32-nvidia-utils nvidia-settings\
""")
        elif gpu == "AMD":
            run_supacman(cmd="""\
-Sy lib32-mesa vulkan-radeon lib32-vulkan-radeon lib32-libva-mesa-driver lib32-mesa-vdpau\
""")
        else:
            run_supacman(cmd="""\
-Sy lib32-mesa vulkan-intel lib32-vulkan-intel\
""")
        run(cmd=f"""\
mkdir -p ~/Games
""")
        run(cmd=f"""\
ln -sf {script_dir}/wine_possibles_fixes.txt ~/Games/wine_possibles_fixes.txt
""")

    # vpn install
    if config["vpn"]:
        run_suparu(cmd="""\
-Sy proton-vpn-gtk-app\
""")

    #
    # Personal projects
    #
    mystuff=r"$HOME/repos/_mystuff"
    localbin=r"$HOME/.local/bin"
    # utils
    run_git(cmd=f"""\
clone --quiet https://github.com/guillaumeboehm/yoro_utils.git "{mystuff}/utils"\
""")

    #
    # setups
    #
    if config["pywal"]:
        run(cmd="""\
mkdir -p "$HOME/.config/wal/repos"\
""")
        run_git(cmd="""\
clone https://github.com/Gremious/discord-wal-theme-template.git\
""", cwd=os.path.expanduser("~/.config/wal/repos"))
        run(cmd="""\
ln -sf $PWD/discord-wal-theme-template/discord-pywal.css ~/.config/wal/templates/colors-discord-pywal.css\
""", cwd=os.path.expanduser("~/.config/wal/repos"))

    #
    # spicetify
    #
    run(cmd="""\
sudo chmod a+wr /opt/spotify\
""")
    run(cmd="""\
sudo chmod a+wr /opt/spotify/Apps -R\
""")
    run(cmd="""\
./pull_themes.sh\
""", cwd=os.path.expanduser('~/.config/spicetify'))
    run(cmd="""\
ln -sf ../../wal_colors.ini color.ini\
""", cwd=os.path.expanduser('~/.config/spicetify/Themes/Comfy'))
    run(cmd="""\
spicetify backup apply\
""", nofail=True)
    if config["pywal"]:
        run(cmd="""\
spicetify config color_scheme wal16\
""", nofail=True)
    else:
        run(cmd="""\
spicetify config color_scheme Comfy\
""", nofail=True)
    run(cmd="""\
spicetify update\
""", nofail=True)

    #xdg-open default setup
    run(cmd="""\
xdg-mime default feh.desktop image/jpeg\
""", nofail=True)
    run(cmd="""\
xdg-mime default feh.desktop image/png\
""", nofail=True)
    run(cmd="""\
xdg-mime default sxiv.desktop image/gif\
""", nofail=True)
    run(cmd="""\
xdg-settings set default-web-browser firefox.desktop\
""", nofail=True)
    run(cmd="""\
xdg-settings set default-url-scheme-handler mailto thunderbird.desktop\
""", nofail=True)

    #
    # systemd stuff
    #
    if config["display"] == "X11":
        pass
        # run(cmd="""\
#sudo systemctl enable "slock@$USER.service"\
#""")
    else:
        run(cmd="""\
systemctl --user enable ydotool\
""")

    run(cmd="""\
sudo systemctl enable sddm\
""")
    run(cmd="""\
sudo systemctl enable NetworkManager avahi-daemon bluetooth autofs\
""")
    run(cmd="""\
systemctl enable --user pipewire.service pipewire-pulse.service trashCleanup.timer wm_event_handler.service\
""")
    run(cmd="""\
sudo systemctl enable "syncthing@$USER.service"\
""")
    run(cmd="""\
systemctl --user enable duplicati.service\
""")
    run(cmd="""\
sudo timedatectl set-local-rtc 1 --adjust-system-clock\
""")
    run(cmd="""\
hyprshade install\
""")
    run(cmd="""\
systemctl enable --user hyprshade.timer hypridle.service\
""")

    #
    # bslock install
    #
    if config["display"] != "wayland":
        run_git(cmd="""\
clone --quiet https://github.com/phenax/bslock.git /tmp/bslock\
""")
        bslock_dir="/tmp/bslock"
        run(cmd=f"""\
patch < "{script_dir}/.bslock.patch"\
""", cwd=bslock_dir)
        run(cmd="""\
sudo make install\
""", cwd=bslock_dir)

    #
    # zsh install
    #
    run(cmd="""\
mkdir -p "$HOME/.config/zsh"\
mkdir -p "$HOME/.local/share/zsh"\
""")
    run_git(cmd="""\
clone --quiet https://github.com/zplug/zplug "$HOME/.local/share/zsh/zplug"\
""")

    #
    # dash as default shell
    #
    run(cmd="""\
sudo ln -sf /bin/dash /bin/sh\
""")

    #
    # qmk/zsa keyboard firmware setup
    #
    run_supacman(cmd="""\
-Sy gtk3 webkit2gtk libusb\
""")

    run(cmd="""\
sudo groupadd plugdev\
""")
    run(cmd="""\
sudo usermod -aG plugdev "$USER"\
""")
    run(cmd="""\
qmk setup guillaumeboehm/qmk_firmware -H ~/.config/qmk/firmware -y\
""")
    run(cmd="""\
ln -sf ~/.config/qmk/firmware/keyboards/massdrop/alt/keymaps/guillaumeboehm ~/.config/qmk/my_conf\
""")
    run(cmd="""\
unzip ~/.config/qmk/hid_listen_1.01.zip -d ~/.config/qmk/\
""")
    run(cmd="""\
make\
""", cwd=os.path.expanduser('~/.config/qmk/hid_listen'))

    run_suparu(cmd="-Syyuu") # last update
    run(cmd="topgrade --disable=restarts --custom-commands=\"NeoVim\" --no-retry --skip-notify -y --no-self-update", nofail=True)

    # Copies the todo on install markdown
    run(cmd=f"""\
cp -r "{script_dir}/.TODO_ON_INSTALL" ~/TODO_ON_INSTALL\
""", nofail=True)

    #
    # cleanup
    #
    run_suparu(cmd="""\
-Sc\
""", nofail=True)
    run(cmd=f"""\
sudo rm -rf $HOME/go\
""", nofail=True)
    # Cleaup some stuff for the setup.sh
    run(cmd=f"""\
sudo rm -r /etc/systemd/system/getty@tty1.service.d\
""", nofail=True)
    run(cmd=f"""\
sudo rm -rf {config_file}\
""", nofail=True)
    run(cmd=f"""\
sudo rm -rf {install_progress}\
""", nofail=True)
    run(cmd=f"""\
mv ~/.bash_profile.bak ~/.bash_profile\
""", nofail=True)

    print("INSTALLATION DONE")

except subprocess.CalledProcessError as e:
    print("Error:", e, "\"")
except Exception as e:
    print("Unknown error:", e)

# Finished

subprocess.run("echo \"$USER ALL=(ALL) ALL\" > \"00_$USER\"", shell=True)
subprocess.run("sudo chown root:root \"00_$USER\"", shell=True)
subprocess.run("sudo mv \"00_$USER\" /etc/sudoers.d/", shell=True)
