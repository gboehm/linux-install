#!/bin/sh

Help()
{
   # Display Help
   echo "Options:"
   echo "h     Print this Help."
   echo "n     No cleaning."
   echo
}

cleaning=true
while getopts ":hn" option; do
    case $option in
        n) # No clean
            cleaning=false
            ;;
        h) # display Help
            Help
            exit
            ;;
        *) # display Help
            Help
            exit
            ;;
    esac
done

script_dir=$(readlink -f "${0}" | sed "s#/$(basename "${0}")##")
config_dir=".config"
dirs=$(ls "$script_dir/$config_dir")

# WARN: No `/*` the quotes prevent globing
excluded='zathura Crow Translate secret'
tracked_ignored='.config/discord/settings.json
.config/discord/Preferences
.config/hypr/configs/monitors.conf
.config/nvim/plugin/packer/packer_compiled.lua
.config/chromium/Local#State'
specials="/etc/zsh/zshenv#.etc/zsh/zshenv
/etc/sddm.conf.d#.etc/
$HOME/.local/share/icons/default#.local/share/icons"

for dir in $dirs; do
    if ! test -L "$HOME/.config/$dir" && ! echo "$excluded" | grep -qP "(^| )$dir($| )"; then
        echo "Copying: $dir"
        cp -rf "$HOME/.config/$dir" "$script_dir/$config_dir/"
    fi
done

for dir in $specials; do
    origin=$(echo "$dir" | awk -F '#' '{print $1}')
    target=$(echo "$dir" | awk -F '#' '{print $2}')
    echo "Copying $origin in $script_dir/$target"
    mkdir -p "$(dirname "$script_dir/$target")"
    cp -rf "$origin" "$script_dir/$target"
done

cd "$script_dir" || exit
for file in $tracked_ignored; do
    file_spaced=$(echo "$file" | sed 's|#| |')
    echo "ignoring '$file_spaced' changes"
    git update-index --assume-unchanged "$file_spaced"
done

if $cleaning; then
    git clean -f
fi

# git restore .config/spicetify .config/fcitx5 # Always versions bump and shit

python "$script_dir/.repos/update_repos.py"
