import subprocess
import json
import os

def main():
    script_dir=os.path.dirname(os.path.abspath(__file__))
    repos_json=os.path.join(script_dir,'repos.json')
    repos_dir=os.path.expanduser('~/repos')

    os.makedirs(repos_dir, exist_ok=True)

    repos={}

    with open(repos_json, mode="r", encoding="utf-8") as read_file:
        repos = json.load(read_file)

    for repo in repos:
        repo_full_path = os.path.join(repos_dir,repo)

        os.makedirs(repo_full_path, exist_ok=True)

        subprocess.run("git init", check=True, shell=True, capture_output=False, cwd=repo_full_path)

        for remote in repos[repo]:
            subprocess.run(f"git remote add {remote} {repos[repo][remote]}", check=False, shell=True, capture_output=False, cwd=repo_full_path)

        # Needs to accept using the ssh key manually here
        subprocess.run("GIT_SSH_COMMAND=\"ssh -o StrictHostKeyChecking=accept-new\" git fetch --all", check=True, shell=True, capture_output=False, cwd=repo_full_path)
        subprocess.run(f"git switch -c master --track origin/master || git switch -c main --track origin/main", check=True, shell=True, capture_output=False, cwd=repo_full_path)

if __name__ == "__main__":
    main()
