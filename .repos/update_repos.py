import os
import json
import subprocess

def main():
    script_dir=os.path.dirname(os.path.abspath(__file__))
    repos_json=os.path.join(script_dir,'repos.json')
    repos_dir=os.path.expanduser('~/repos')

    # { "repo_path": { "remote_name":  "url", "remote2": "url2" }, "repo2":... }
    repos={}

    # Get all the repos
    for root, dirs, _ in os.walk(repos_dir):
        for dir in dirs:
            if(dir == ".git"):
                is_subrepository = False
                for repo in repos:
                    full_repo_path=os.path.join(repos_dir,repo)
                    if os.path.commonpath([ root, full_repo_path ]) == full_repo_path:
                        is_subrepository = True
                if not is_subrepository:
                    rel_path=root.lstrip(repos_dir)
                    repos[rel_path] = None

    for repo_rel_path in repos:
        repo = os.path.join(repos_dir, repo_rel_path)
        remotes={}

        remotes_list = subprocess.run("git remote show",check=True, shell=True, capture_output=True, cwd=repo).stdout.decode('UTF-8').splitlines()
        for remote in remotes_list:
            url = subprocess.run(f"git remote get-url {remote}",check=True, shell=True, capture_output=True, cwd=repo).stdout.decode('UTF-8').rstrip()

            remotes[remote] = url

        repos[repo_rel_path] = remotes

    with open(repos_json, mode="w", encoding="utf-8") as write_file:
        json.dump(repos, write_file)

if __name__ == "__main__":
    print("Updating repos...")
    main()
    print("Repos updated.")
