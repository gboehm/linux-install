#
# ~/.bashrc
#

# If not running interactively, don't do anything
. "$HOME/.local/share/shinit"
export HISTFILE="$XDG_STATE_HOME/bash/history"

[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
export _JAVA_AWT_WM_NONREPARENTING=1
export CHROME_EXECUTABLE="/bin/chromium"

export PATH=$PATH:/home/yoro/.config/spicetify
