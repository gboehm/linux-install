#!/usr/bin/env python

# Lists the layouts available
# Inputs for choice and writes the choice in the 'current_layout' file

import re
import os
import typing

script_path=os.path.realpath(__file__)
script_dir=os.path.dirname(os.path.realpath(__file__))

layouts_dir=f"{script_dir}/layouts"

current_layout_filename='current_layout' # FIXME: To change
current_layout_path=f"{script_dir}/{current_layout_filename}"

# INFO: Returns the name of the chosen layout
def chose_layout() -> str:
    # Get the list of layouts
    layouts:typing.List[str] = ['none']
    for entry in os.listdir(layouts_dir):
        if os.path.isfile(f"{layouts_dir}/{entry}") and entry.endswith(".layout"):
            layouts.append(entry.replace('.layout', ''))

    # Get the name of the current layout
    current_layout:(str|None)=None
    if os.path.isfile(current_layout_path) and not os.path.islink(current_layout_path):
        current_layout_file = open(current_layout_path, 'r')
        if not current_layout_file:
            print(f"Could not open current layout file '{current_layout_path}' to read")
            exit(1)
        current_layout = current_layout_file.read().strip('\n').strip(' ')

    if not current_layout:
        print(f"Could not get current layout from {current_layout_path}")
        exit(1)

    print(f"Current layout: {current_layout}\n")

    choice = ""
    while ( not re.fullmatch(r'[0-9]+', choice) ) or int(choice) < 1 or int(choice) > len(layouts):
        for i in range(len(layouts)):
            print(f"{i+1}. {layouts[i]}")

        choice = input("\nChose layout[cancel]: ")
        if len(choice) == 0:
            print("Canceling...")
            exit(1)
    return layouts[int(choice)-1]

if __name__ == "__main__":


    # Input the user to select the layout to set
    layout = chose_layout()
    print(f"layout chosen: {layout}")

    # Write it to the current layout file
    current_layout_file = None
    if os.path.isfile(current_layout_path) and not os.path.islink(current_layout_path):
        current_layout_file = open(current_layout_path, 'w')
        if not current_layout_file:
            print(f"Could not open current layout file '{current_layout_path}' to write")
            exit(1)

        current_layout_file.write(layout)
        current_layout_file.close()
