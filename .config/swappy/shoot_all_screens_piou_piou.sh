#!/bin/sh

monitors=$(hyprctl monitors -j)
x=$(echo "$monitors" | jq -r ".[] | [.x]" | jq -s "add")
y=$(echo "$monitors" | jq -r ".[] | [.y]" | jq -s "add")

SCREENSHOT_MIN_X=$(echo "$x" | jq -r "min");
SCREENSHOT_MIN_Y=$(echo "$y" | jq -r "min");

SCREENSHOT_WIDTH=$(echo "$monitors" | jq -r ".[] | select(.x == $(echo "$x" | jq -r "max"))" | jq -r ".x + .width - $SCREENSHOT_MIN_X");
SCREENSHOT_HEIGHT=$(echo "$monitors" | jq -r ".[] | select(.y == $(echo "$y" | jq -r "max"))" | jq -r ".y + .height - $SCREENSHOT_MIN_Y");
grim -g "$SCREENSHOT_MIN_X,$SCREENSHOT_MIN_Y ${SCREENSHOT_WIDTH}x$SCREENSHOT_HEIGHT" -

