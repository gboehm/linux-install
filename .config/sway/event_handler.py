from i3ipc import Connection, Event

import gamescontrol

# https://man.archlinux.org/man/sway-ipc.7.en
# https://i3ipc-python.readthedocs.io/en/latest/index.html

def getClass(e):
    windowclass = e.ipc_data['container']['app_id']
    if(windowclass == None):
        windowclass = e.ipc_data['container']['window_properties']['class']
    return windowclass


def onOpenWindow(_, e):
    windowId = e.ipc_data['container']['window']
    windowClass = getClass(e)

    gamescontrol.addGame(windowId, windowClass)

def onCloseWindow(_, e):
    windowId = e.ipc_data['container']['window']

    gamescontrol.removeGame(windowId)


i3 = Connection()

i3.on(Event.WINDOW_NEW, onOpenWindow)
i3.on(Event.WINDOW_CLOSE, onCloseWindow)

print("Handling sway events...")

i3.main()
