#!/bin/sh

if [ "$(protonvpn-cli status | xargs)" = "No active Proton VPN connection." ]; then
    notify-send -t 1000 "Connecting VPN"
    protonvpn-cli c -f || notify-send -t 2000 "Failed to connect VPN"
else
    notify-send -t 1000 "Disconnecting VPN"
    protonvpn-cli d && notify-send -t 2000 "VPN Disconnected"
fi
