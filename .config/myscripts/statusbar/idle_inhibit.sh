#!/bin/sh

# Is location cache persistent or not
location="/tmp"
if [ -n "$IDLE_INHIBIT_CACHE_PERSISTENT" ]; then
    location="${XDG_CACHE_HOME:-$HOME/.cache}"
fi
file="$location/statusbar/idle_inhibit"

action="$1"

# Initial
if [ ! -f "$file" ]; then
    mkdir -p "$(dirname "$file")"

   printf "1\nsleep" > "$file"
fi

case "$action" in
    "cycle")
        # Cycle
        case "$(head -1 "$file")" in
            1)
               printf "2\nscreensaver" > "$file"
                ;;
            2)
               printf "3\nwake" > "$file"
                ;;
            3)
               printf "1\nsleep" > "$file"
                ;;
        esac
        ;;
    "query")
        if [ ! -f "$file" ]; then
            printf "sleep";
        else
            tail -1 "$file";
        fi
        ;;
esac

exit 0;
