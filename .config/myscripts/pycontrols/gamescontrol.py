import os
import re

import threading

# Regex matching game window classes
__games_classes=["steam_proton", "steam_app.*", "gamescope", "spring", "SDL Application", "valheim.x86_64"]
__ignored_programs=[".*wineboot\\.exe.*"]
# A list of all the currently running games window ids
__running_games=[]

__stopping=False

def isGameRunning() -> bool:
    return len(__running_games) > 0

def addGame(windowId: str, windowClass: str) -> bool:
    global __running_games, __games_classes, __ignored_programs
    ret=False
    running = isGameRunning()
    if(not windowId in __running_games):
        # try adding the game only if the window class matches a possible game regex
        for gameclass in __games_classes:
            if re.compile(gameclass).match(windowClass):
                # abort if the progam should be ignored
                for ignored in __ignored_programs:
                    if re.compile(ignored).match(windowClass):
                        return False
                __running_games.append(windowId)
                break
    if(not running and isGameRunning()):
        __onStartedGaming()
        ret=True
    return ret


def removeGame(windowId: str) -> bool:
    global __running_games, __stopping
    ret=False
    running = isGameRunning()
    if windowId in __running_games:
        __running_games.remove(windowId)
    if(running and not isGameRunning() and not __stopping):
        __onStoppedGaming()
        ret = True
    return ret


def __onStartedGaming():
    global __stopping
    __stopping=False
    os.system("mcontrolcenter -B ON")

def __onStoppedGaming():
    global __stopping
    __stopping=True
    t = threading.Timer(5.0, __stop)
    t.start()

def __stop():
    global __stopping
    if __stopping:
        os.system("mcontrolcenter -B OFF")
        __stopping=False

