#!/bin/sh

export ANDROID_HOME="$XDG_DATA_HOME/android"
export ANDROID_HOME_USER="$XDG_DATA_HOME/android"
export ANDROID_USER_HOME="$XDG_DATA_HOME/android"
export GOPATH="$XDG_DATA_HOME/go"
export ANSIBLE_HOME="$XDG_DATA_HOME/ansible"
export ANSIBLE_HOME="$XDG_DATA_HOME/ansible"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export DOTNET_CLI_HOME="$XDG_DATA_HOME/dotnet"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export GRADLE_USER_HOME="$XDG_DATA_HOME/gradle"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export LESSHISTFILE="$XDG_STATE_HOME/less/history"
export NUGET_PACKAGES="$XDG_CACHE_HOME/NuGetPackages"
export NVM_DIR="$XDG_DATA_HOME/nvm"
export OMNISHARPHOME="$XDG_CONFIG_HOME/omnisharp"
export PYLINTHOME="$XDG_CACHE_HOME/pylint"
export PYTHONSTARTUP="$HOME/.python/pythonrc"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export WINEPREFIX="$XDG_DATA_HOME/wine"
export ERRFILE="$XDG_CACHE_HOME/X11/xsession-errors"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export DOCKER_CONFIG="$XDG_CONFIG_HOME/docker"
export _Z_DATA="$XDG_DATA_HOME/z"
export _ZL_DATA="$XDG_DATA_HOME/zlua"
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
export RENPY_PATH_TO_SAVES="$XDG_DATA_HOME"
export NODE_REPL_HISTORY="$XDG_DATA_HOME"/node_repl_history
export FVM_CACHE_PATH="$XDG_CACHE_HOME/fvm"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME/java -Djavafx.cachedir=${XDG_CACHE_HOME}/openjfx"

# decluttering aliases
alias adb='HOME="$XDG_DATA_HOME/android" adb'
alias monerod='monerod --data-dir "$XDG_DATA_HOME/bitmonero"'
alias prismlauncher='prismlauncher -d "$XDG_DATA_HOME/minecraft"'
alias wget='wget --hsts-file="$XDG_DATA_HOME/wget-hsts"'
