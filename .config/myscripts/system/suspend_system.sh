#!/bin/sh

# Is location cache persistent or not
idle_inhibit_location="/tmp"
if [ -n "$IDLE_INHIBIT_CACHE_PERSISTENT" ]; then
    idle_inhibit_location="${XDG_CACHE_HOME:-$HOME/.cache}"
fi
idle_inhibit_file="$idle_inhibit_location/statusbar/idle_inhibit"

inhibit=false
if [ -f "$idle_inhibit_file" ] && [ ! "$(tail -1 "$idle_inhibit_file")" = "sleep" ]; then
    inhibit=true
fi

if [ $inhibit = false ]; then
    systemctl suspend
fi
