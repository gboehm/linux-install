#!/usr/bin/env python3

from optparse import OptionParser
import subprocess

parser = OptionParser(usage="usage: %prog package")

(options, args) = parser.parse_args()

if __name__ == "__main__":
    if len(args) != 1:
        parser.print_help()
        exit(1)

    proc = subprocess.run(["pactree","--reverse",args[0]], stdout=subprocess.PIPE)
    tree = proc.stdout.decode("utf-8").splitlines()

    proc = subprocess.run(["paru","-Qqm"], stdout=subprocess.PIPE)
    aurs = proc.stdout.decode("utf-8").split()

    for dep in tree:
        clean_dep = dep.split('─')[1] if len(dep.split('─')) > 1 else dep

        if clean_dep in aurs:
            dep = dep + " [aur]"
        else:
            dep = dep + " [official]"
        print(dep)

