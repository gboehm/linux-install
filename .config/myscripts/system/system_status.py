#!/usr/bin/env python3

import subprocess

#
# Displays some system info on the first workspace
#

def open_window(command, match, workspace, width, height, x_offset, y_offset):
    if len([proc for proc in subprocess.check_output(['ps', 'uafxw']).splitlines() if match in proc]) == 0:
        subprocess.run(["hyprctl", "dispatch", "exec", f"[workspace {workspace} silent;float;size {width}% {height}%;move {x_offset}% {y_offset}%] {command}"])

if __name__ == "__main__":
    target_workspace = 1

    w_width=45
    w_height=43.5
    w_xOffset=53.5
    w_yOffset=55
    command="kitty --class=Kitty_neofetch -o allow_remote_control=yes -o enabled_layouts=tall -c ~/.config/neofetch/kitty.conf /bin/zsh -c 'neofetch; kitty @ scroll-window 10p-; sleep infinity'"
    ps_match=b'kitty --class=Kitty_neofetch'
    open_window(command, ps_match, target_workspace, w_width, w_height, w_xOffset, w_yOffset)

    w_width=52
    w_height=85
    w_xOffset=0.5
    w_yOffset=9
    command="kitty --class=Kitty_btop btop"
    ps_match=b'kitty --class=Kitty_btop'
    open_window(command, ps_match, target_workspace, w_width, w_height, w_xOffset, w_yOffset)

    w_width=43
    w_height=47
    w_xOffset=55
    w_yOffset=6
    command="kitty --class=Kitty_nvtop nvtop"
    ps_match=b'kitty --class=Kitty_nvtop'
    open_window(command, ps_match, target_workspace, w_width, w_height, w_xOffset, w_yOffset)
