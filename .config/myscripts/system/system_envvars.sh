#!/bin/sh
# shellcheck disable=SC2034  # Unused variables

set -a

EDITOR=vim
TERM=kitty
XCURSOR_PATH=/usr/share/icons:$HOME/.local/share/icons

WINEPREFIX=$HOME/.local/share/wine

GTK_THEME=Arc-Dark
QT_QPA_PLATFORMTHEME=qt5ct

PYTHONPATH="$PYTHONPATH:$HOME/.config/myscripts/pycontrols"

PATH="$HOME/.local/bin:$PATH"
