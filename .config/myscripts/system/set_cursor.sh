echo "XCursor name: "
read -r xcursor_name

echo "Hyprcursor name: "
read -r hyprcursor_name

echo "Cursor size: "
read -r cursor_size

file=~/.xprofile
echo "Write in $file"
sed -i -E "s/^(export XCURSOR_THEME=).*$/\1$xcursor_name/" "$file"
sed -i -E "s/^(export XCURSOR_SIZE=).*$/\1$cursor_size/" "$file"

file=~/.Xresources
echo "Write in $file"
sed -i -E "s/^(Xcursor.theme: ).*$/\1$xcursor_name/" "$file"
sed -i -E "s/^(Xcursor.size: ).*$/\1$cursor_size/" "$file"

file=~/.config/gtk-2.0/gtkrc
echo "Write in $file"
sed -i -E "s/^(gtk-cursor-theme-name).*$/\1=\"$xcursor_name\"/" "$file"
sed -i -E "s/^(gtk-cursor-theme-size).*$/\1=$cursor_size/" "$file"

file=~/.config/gtk-3.0/settings.ini
echo "Write in $file"
sed -i -E "s/^(gtk-cursor-theme-name).*$/\1=$xcursor_name/" "$file"
sed -i -E "s/^(gtk-cursor-theme-size).*$/\1=$cursor_size/" "$file"

file=~/.config/gtk-4.0/settings.ini
echo "Write in $file"
sed -i -E "s/^(gtk-cursor-theme-name).*$/\1=$xcursor_name/" "$file"
sed -i -E "s/^(gtk-cursor-theme-size).*$/\1=$cursor_size/" "$file"

file=~/.local/share/icons/default/index.theme
echo "Write in $file"
sed -i -E "s/^(Inherits).*$/\1=$xcursor_name/" "$file"

file=/etc/sddm.conf.d/00_theme.conf
echo "Write in $file"
doas sed -i -E "s/^(CursorTheme).*$/\1=$xcursor_name/" "$file"
doas sed -i -E "s/^(CursorSize).*$/\1=$cursor_size/" "$file"

file=~/.config/hypr/configs/envvars.conf
echo "Write in $file"
sed -i -E "s/^(env[[:space:]]*=[[:space:]]*XCURSOR_THEME,).*$/\1$xcursor_name/" "$file"
sed -i -E "s/^(env[[:space:]]*=[[:space:]]*XCURSOR_SIZE,).*$/\1$cursor_size/" "$file"
sed -i -E "s/^(env[[:space:]]*=[[:space:]]*HYPRCURSOR_THEME,).*$/\1$hyprcursor_name/" "$file"
sed -i -E "s/^(env[[:space:]]*=[[:space:]]*HYPRCURSOR_SIZE,).*$/\1$cursor_size/" "$file"
