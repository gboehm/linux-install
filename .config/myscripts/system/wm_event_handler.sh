#!/bin/sh

if command swaymsg -t get_version > /dev/null; then
    echo "sway ok"
    python "$HOME/.config/sway/event_handler.py"
elif command hyprctl version > /dev/null; then
    echo "hypr ok"
    python "$HOME/.config/hypr/scripts/event_handler.py"
else
    echo "No window manager instance found, stopping"
    exit 0
fi
