#!/bin/sh

STATUS_FILE="${XDG_RUNTIME_DIR:-/tmp}/$1.status"

# Set device to be toggled
if [ $# -ne 1 ]; then
    echo "Needs the device as parameter (touchpad)"
    notify-send "Failed to toggle '$DEVICE'"
    exit 1
fi

DEVICE=$(echo "$1" | tr '[:lower:]' '[:upper:]')
HYPRLAND_DEVICE_NAME=$(eval "echo \"\$HYPRLAND_DEVICE_$DEVICE\"")
if [ -z "$HYPRLAND_DEVICE_NAME" ]; then
    echo "Could not find a device name for the '$DEVICE' Device"
    notify-send "Failed to toggle '$DEVICE'"
    exit 1
fi
if ! hyprctl devices | grep "$HYPRLAND_DEVICE_NAME" > /dev/null; then
    echo "Device '$HYPRLAND_DEVICE_NAME' not found"
    notify-send "Failed to toggle '$DEVICE'"
    exit 1
fi

if [ -z "$XDG_RUNTIME_DIR" ]; then
    XDG_RUNTIME_DIR=/run/user/$(id -u)
    export XDG_RUNTIME_DIR
fi

enable_device() {
    printf "true" >"$STATUS_FILE"
    notify-send -u normal "Enabling Device '$DEVICE'"
    hyprctl keyword "\$$DEVICE""_ENABLED" "true" -r
}

disable_device() {
    printf "false" >"$STATUS_FILE"
    notify-send -u normal "Disabling Device '$DEVICE'"
    hyprctl keyword "\$$DEVICE""_ENABLED" "false" -r
}

if ! [ -f "$STATUS_FILE" ]; then
  enable_device
else
  if [ "$(cat "$STATUS_FILE")" = "true" ]; then
    disable_device
  elif [ "$(cat "$STATUS_FILE")" = "false" ]; then
    enable_device
  fi
fi
