#!/bin/sh

# Is location cache persistent or not
idle_inhibit_location="/tmp"
if [ -n "$IDLE_INHIBIT_CACHE_PERSISTENT" ]; then
    idle_inhibit_location="${XDG_CACHE_HOME:-$HOME/.cache}"
fi
idle_inhibit_file="$idle_inhibit_location/statusbar/idle_inhibit"

inhibit=false
if [ -f "$idle_inhibit_file" ] && [ "$(tail -1 "$idle_inhibit_file")" = "wake" ]; then
    inhibit=true
fi

if [ $inhibit = false ]; then
    action="$1"
    case "$action" in
        "on")
            brightnessctl -rd rgb:kbd_backlight
            meestic -i 10;
            ;;
        "off")
            brightnessctl -sd rgb:kbd_backlight set 0; # set monitor backlight to minimum, avoid 0 on OLED monitor.
            meestic -i 0;
            ;;
    esac
fi
