#!/bin/sh

pidfile_pre="/tmp/screensaverpids"
pidfile_suf=".mystuff"

for f in "$pidfile_pre"*; do
    if [ "$( expr "$f" : ".*$pidfile_suf$" )" -ne 0 ]; then
        while read -r pid; do
            if [ -n "$pid" ] && [ "$(ps --pid "$pid" --no-headers | awk '{ print $4 }')" = "mpv" ]; then
                kill "$pid" > /dev/null
            fi
        done < "$f"

        rm "$f" 2> /dev/null
    fi
done
