#!/bin/sh

if [ "$#" -lt 1 ]; then echo "Not enough arguments"; exit 1; fi
if [ ! -x "$(command -v mpv)" ]; then echo "Mpv not installed"; exit 1; fi
video="$1"
if [ ! -f "$video" ]; then echo "Video not found"; exit 1; fi

pidfile_pre="/tmp/screensaverpids"
pidfile_suf=".mystuff"

rotation() {
    transform=$(hyprctl monitors -j | jq -r ".[] | select(.name == \"$(hyprctl workspaces -j | jq -r ".[] | select(.id == $1) | .monitor")\") | .transform" | grep -oE "[0-9]+")

    case $transform in
        1) echo "90" ;;
        2) echo "180" ;;
        3) echo "270" ;;
        *) echo "0" ;;
    esac
}

launch() {
    ws="$1"
    mpv_count="$2"
    ws_count="$3"
    num="$4"

    start="$(awk ' BEGIN \
        {	srand('"(( $(date +%s%N) + $ws * 10000 ))"')
        printf( "%d", 100 * rand() )
    }')"

    hyprctl dispatch exec "[fullscreen;noanim;workspace $ws silent;idleinhibit none] mpv --no-audio --stop-screensaver=no --panscan=1 --video-rotate=$(rotation "$ws") --loop=inf --start=$start% '$video' --really-quiet --no-terminal" > /dev/null
    while true; do
        sleep 0.1
        if [ "$(pgrep "mpv" | wc -l )" -ge "$(( mpv_count + ws_count ))" ]; then break; fi
    done;
    mpvpid="$(pgrep "mpv" | tail -n"$num" | tac | tail -n1 )"

    echo "$mpvpid" > "$pidfile_pre$num$pidfile_suf"
}

workspaces="$(hyprctl monitors -j | jq -r ".[] | .activeWorkspace.id")"

mpv_count="$(pgrep "mpv" | wc -l )"
ws_count=$(echo "$workspaces" | wc -l)
num=1
for ws in $workspaces; do
    launch "$ws" "$mpv_count" "$ws_count" "$num" &
    num=$(( num + 1 ))
done
