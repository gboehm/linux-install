#!/bin/sh

lockfile="/tmp/hyprlocked.mystuff"
if [ -f "$lockfile" ]; then
    echo "hyprlock already running, exiting..."
    exit 1
fi

# Is location cache persistent or not
idle_inhibit_location="/tmp"
if [ -n "$IDLE_INHIBIT_CACHE_PERSISTENT" ]; then
    idle_inhibit_location="${XDG_CACHE_HOME:-$HOME/.cache}"
fi
idle_inhibit_file="$idle_inhibit_location/statusbar/idle_inhibit"

inhibit=false
if [ -f "$idle_inhibit_file" ] && [ "$(tail -1 "$idle_inhibit_file")" = "wake" ]; then
    inhibit=true
fi

if [ $inhibit = false ] && not pidof hyprlock > /dev/null; then
    eval "$HOME/.config/myscripts/screensaver/launch_screensaver.sh \"$HOME/Videos/screensaver_lowres.mp4\""
    touch "$lockfile"
    hyprlock;
    rm "$lockfile"
    eval "$HOME/.config/myscripts/screensaver/kill_screensaver.sh"
fi
