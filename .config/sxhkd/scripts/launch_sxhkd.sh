#!/bin/sh

## for super keybind alone
# pkill -x xcape > /dev/null; xcape -e $(awk '{print}' ORS=';' ~/.config/sxhkd/xcape.conf)
pkill -x sxhkd > /dev/null; sxhkd -m 1 &
python ~/.config/sxhkd/scripts/shortcuts_list.py ~/.config/sxhkd/sxhkdrc > ~/.config/sxhkd/scripts/shortcuts_list
