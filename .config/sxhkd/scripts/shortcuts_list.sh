#!/bin/sh

cat ~/.config/sxhkd/scripts/shortcuts_list | yad --width=$(~/.config/bspwm/scripts/current_monitor_size.sh width 0.9) --height=$(~/.config/bspwm/scripts/current_monitor_size.sh height 0.9) --html --no-buttons --disable-search --borders=0 --sticky --on-top --skip-taskbar --close-on-unfocus 2>&1
