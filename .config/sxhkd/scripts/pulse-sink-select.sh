#!/bin/sh
# changes default sink and moves all streams to that sink

if ! eval "command -v ponymix &> /dev/null"; then
    notify-send "Error: Install ponymix before running"
    exit 1
fi

currentSink="$(pacmd info | grep -Po "(?<=$(pacmd info | grep -Po "(?<=Default sink name: ).*")/#\d: ).*")"
ignoredSinks="GP104|Navi|<SomeOtherStuff>"
sink=$(ponymix -t sink list | awk '/^sink/ {s=$1" "$2;getline;gsub(/^ +/,"",$0);print s" "$0}' | grep -vE "$ignoredSinks" | grep -v "$currentSink" | rofi -dmenu -p 'pulseaudio sink:' -i -config ~/.config/rofi/config-rbw.rasi | grep -Po '[0-9]+(?=:)') &&

ponymix set-default -d "$sink" &&
for input in $(ponymix list -t sink-input | grep -Po '[0-9]+(?=:)'); do
	echo "$input -> $sink"
	ponymix -t sink-input -d "$input" move "$sink"
done
