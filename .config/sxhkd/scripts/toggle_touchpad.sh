#!/bin/sh

TOUCHPAD='ELAN0300:00 04F3:30AA Touchpad'

if [ $(xinput list-props "$TOUCHPAD" | grep 'Device Enabled' | gawk -F ':' '{ print $2 }' | sed 's/[[:space:]]*//g') -eq 0 ]; then
    xinput enable "$TOUCHPAD" && notify-send "Touchpad enabled"
else
    xinput disable "$TOUCHPAD" && notify-send "Touchpad disabled"
fi
