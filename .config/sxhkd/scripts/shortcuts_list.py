from string import Template
import sys,re

## HTML Styling
print('''\
<!DOCTYPE html>
<html>
    <head>
    </head>

    <style type="text/css">
        .wrapper {
          display: grid;
          grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
          grid-gap: 5px;
          grid-auto-flow: dense;
          list-style: none;
          padding: 0;
        }
        .wrapper li {
            width:100%;
            display: block;
        }
        * {
          background-color: #2d2a2e;
          color: #fcfcfa;
        }
        h2 {
            padding: 0;
            margin: 0;
            text-align:center;
            color: #ff6188;
        }
        .group {
            color: #fc9867;
            font-weight: bold;
            font-size: 1.2em;
            margin-bottom: -10px;
        }
        .bind {
            color: #7bd88f;
            font-style: italic;
        }
        .action {
            color: #40b2bf;
        }
        .command {
            color: #f0c674;
        }
        .icon {
            color: #ab9df2;
            position: absolute;
            display: inline-block;
            transform:translate(-15px, 3px);
            background: transparent;
        }
        span {
            margin: 0;
            padding: 0;
        }
        div {
            width: 100%;
            overflow-wrap: break-word;
        }
    </style>

    <body class="body">
        <h2>BSPWM Shortcuts</h2>

''')

group_template = Template('''\
        <span class='group'>### $title ###</span>

        <ul class="wrapper">
''')
bind_template = Template('''\
            <li><div>
                <span class='action'>- $name</span>
                <br><span>&ensp;&ensp;&ensp;</span><span class='icon'></span>
                <span class='bind'>$bind</span>
            </div></li>
''')
group_close = '''\
        </ul>
'''

## Content generation
groups_regex = r'^#\s*\n#\s*(?P<title>[^\n]*)\n#\s*(?P<keybinds>[\w\W]*?(?=^#\s*\n#\s*[^\n]*\n#|#!!EOF))'
binds_regex = r'(?:[\w\W]*?(?=^#))^#\?\?\s*(?P<keybindName>[^\n]*(?=\n[\w{]))(?:[\w\W]*?(?=^[\w{]))(?P<keybindBind>^[^\n]*)\s*(?P<keyBindCommand>[^\n]*)'
groups_pattern = re.compile(groups_regex, re.MULTILINE)
binds_pattern = re.compile(binds_regex, re.MULTILINE)

groups = []
with open(sys.argv[1], 'r') as file:
    sxhkdrc = file.read()

    for (title,match) in re.findall(groups_pattern, sxhkdrc):
        group = {'title':title, 'binds':[]}
        for (name, bind, command) in re.findall(binds_pattern, match):
            bind = {'name':name, 'bind':bind, 'command':command}
            group['binds'].append(bind)
        groups.append(group)

for group in groups:
    print(group_template.substitute(title=group['title']))
    for bind in group['binds']:
        print(bind_template.substitute(name=bind['name'], bind=bind['bind'].replace('super + ctrl + shift + alt', 'META')))
    print(group_close)

print('''\
    </body>
</html>''')
