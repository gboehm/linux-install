#!/bin/sh

nd="$HOME/.local/share/nerd-dictation/nerd-dictation"
# nd_begin_opts="--vosk-model-dir=$HOME/.config/nerd-dictation/vosk-model-small-en-us-0.15"
tmp_output="/tmp/nerd-dictation.tmp"
target="$HOME/.TODO"
info_text="Recording TODO"

if pgrep -f "nerd-dictation begin"; then
    pkill -f "^zenity.*--text=$info_text\$"
    $nd end
else
    rm -f $tmp_output
    bspc rule -a Zenity -o state=floating border=off sticky=on focus=off center=off rectangle=200x60+0+0
    zenity --info --no-wrap --no-markup --text="$info_text" &
    $nd begin $nd_begin_opts --output STDOUT > $tmp_output && [ -s $tmp_output ] && echo -n '- ' >> $target && cat $tmp_output >> $target && echo -n '\n' >> $target &
fi
