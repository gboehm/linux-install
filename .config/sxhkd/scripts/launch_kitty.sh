#!/bin/sh

HOSTNAME=$(cat /proc/sys/kernel/hostname)
cwd="$(xprop -id "$(bspc query -N -n)" | grep -P "WM_NAME\(STRING\)" | grep -Po "(?<=\= \").*(?=\"$)")"
if [ ! -f $cwd ]; then
    kitty -d $cwd &
else
    kitty -d ~ &
fi
