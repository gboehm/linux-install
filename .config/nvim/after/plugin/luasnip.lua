require("luasnip.loaders.from_vscode").lazy_load()

local ls = require("luasnip")

vim.keymap.set({ "i" }, '<Tab>', function()
  if ls.expand_or_jumpable() then
    ls.expand_or_jump()
  else
    local key = vim.api.nvim_replace_termcodes("<Tab>", true, false, true)
    vim.api.nvim_feedkeys(key, 'n', false)
  end
end, { silent = true })

vim.keymap.set({"i", "s"}, "<S-Tab>", function() ls.jump( 1) end, {silent = true})

vim.keymap.set({"s"}, "<Tab>", function() ls.jump(1) end, {silent = true})

vim.keymap.set({"i", "s"}, "<C-E>", function()
	if ls.choice_active() then
		ls.change_choice(1)
	end
end, {silent = true})

-- Use framework snippets
-- e.g. ls.filetype_extend("ruby", {"rails"})


-- custom snippets

ls.add_snippets("lua", {
	ls.snippet("ternary", {
		-- equivalent to "${1:cond} ? ${2:then} : ${3:else}"
		ls.insert_node(1, "cond"), ls.text_node(" ? "), ls.insert_node(2, "then"), ls.text_node(" : "), ls.insert_node(3, "else")
	})
})
