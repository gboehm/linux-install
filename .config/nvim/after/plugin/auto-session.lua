local autosession = require("auto-session")

autosession.setup {
    auto_save_enabled = true,
    auto_session_use_git_branch = true,
    auto_session_suppress_dirs = { "~/", "~/Projects", "~/Downloads", "/" },
    auto_restore_enabled = true,
    session_lens = {
        path_display = {'shorten'},
    },
}
vim.o.sessionoptions="blank,buffers,curdir,folds,help,tabpages,winsize,winpos,terminal,localoptions"
-- vim.cmd('au QuitPre * SaveSession')
