local dap = require("dap")
local dapui = require("dapui")
local dappython = require("dap-python")

dapui.setup()
dappython.setup('~/.config/virtualenvs/debugpy/bin/python')

dap.env = function()
    local variables = {}
    for k, v in pairs(vim.fn.environ()) do
        table.insert(variables, string.format("%s=%s", k, v))
    end
    return variables
end

dap.listeners.after.event_initialized["dapui_config"] = function()
    dapui.open()
end
dap.listeners.before.event_terminated["dapui_config"] = function()
    dapui.close()
end
dap.listeners.before.event_exited["dapui_config"] = function()
    dapui.close()
end

dap.adapters.lldb = {
    type = 'executable',
    command = '/usr/bin/lldb-dap',
    name = 'lldb'
}
dap.adapters.dgb = {
    type = 'executable',
    command = '/usr/bin/gdb',
    name = 'gdb'
}
dap.adapters.nlua = function(callback, config)
    callback({ type = 'server', host = config.host or "127.0.0.1", port = config.port or 8086 })
end

dap.configurations.cpp = {
    {
        name = 'Launch',
        type = 'lldb',
        request = 'launch',
        program = function()
            return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
        end,
        cwd = '${workspaceFolder}',
        stopOnEntry = false,
        args = {},
        initCommands = function()
            return {'command script import "/home/yoro/repos/_config/kde_formatters/plugins/lldb/formatters/all.py"'}
            -- return {'command script import "/home/yoro/repos/_config/Qt6Renderer/python/lldb/qt6renderer"'}
        end,
    },
}

-- If you want to use this for Rust and C, add something like this:

dap.configurations.c = dap.configurations.cpp
dap.configurations.rust = {
    -- ... the previous config goes here ...,
    initCommands = function()
        -- Find out where to look for the pretty printer Python module
        local rustc_sysroot = vim.fn.trim(vim.fn.system('rustc --print sysroot'))

        local script_import = 'command script import "' .. rustc_sysroot .. '/lib/rustlib/etc/lldb_lookup.py"'
        local commands_file = rustc_sysroot .. '/lib/rustlib/etc/lldb_commands'

        local commands = {}
        local file = io.open(commands_file, 'r')
        if file then
            for line in file:lines() do
                table.insert(commands, line)
            end
            file:close()
        end
        table.insert(commands, 1, script_import)

        return commands
    end,
    -- ...,
}

dap.configurations.lua = {
    {
        type = "nlua",
        request = "attach",
        name = "Attach to running Neovim instance",
    }
}

