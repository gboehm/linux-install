local regexplainer = require("regexplainer")

regexplainer.setup({
    filetypes = {
        'js',
        'tsx',
        'cpp',
        'c',
        'rs',
        'py',
        'sh'
    }
})

