local configs = require('nvim-treesitter.configs')

configs.setup({
    ensure_installed = { 'lua', 'python', 'c', 'cpp', 'rust', 'cmake', 'dart',
    'bash', 'css', 'html', 'htmldjango', 'git_rebase', 'gitignore', 'gitattributes',
    'javascript', 'json', 'latex', 'make', 'markdown', 'markdown_inline', 'meson', 'ninja',
    'rasi', 'yaml', 'regex', 'hyprlang', 'vim'},
    auto_install = true,
    highlight = {
        enable = true,
        disable = { 'gitcommit' },
    },
    indent = {
        enable = true,
    },
})

vim.treesitter.language.register('xml', 'html')
vim.cmd('set foldmethod=expr')
vim.cmd('set foldexpr=nvim_treesitter#foldexpr()')
vim.cmd('set nofoldenable')

-- treesitter-context
local context = require('treesitter-context')

context.setup{
    enable = true,
    max_lines = 10,
}
-- Some colors to distinguish the header zone
vim.cmd('hi TreesitterContext guibg=Black')
vim.cmd('hi TreesitterContextBottom gui=underline guisp=Grey')
vim.cmd('hi link TreesitterContextLineNumber DiagnosticInfo')
