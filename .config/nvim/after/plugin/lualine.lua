local function maximize_status()
    return vim.t.maximized and '' or ''
end

require("lualine").setup({
        options = {
            icons_enabled = true,
            theme = 'monokai-pro',
            component_separators = { left = '', right = ''},
            section_separators = { left = '', right = ''},
            disabled_filetypes = {
                statusline = {},
                winbar = {},
            },
            ignore_focus = {},
            always_divide_middle = true,
            globalstatus = false,
            refresh = {
                statusline = 1000,
                tabline = 1000,
                winbar = 1000,
            }
        },
        sections = {
            lualine_a = {maximize_status, 'mode'},
            lualine_b = {'branch', 'diff', 'diagnostics'},
            lualine_c = {{require('auto-session.lib').current_session_name},{'filename' , path = 1}},
            lualine_x = { require('NeoComposer.ui').status_recording, 'encoding', 'fileformat', 'filetype'},
            lualine_y = {'progress'},
            lualine_z = {'location'}
        },
        inactive_sections = {
            lualine_a = {},
            lualine_b = {},
            lualine_c = {'filename'},
            lualine_x = {'location'},
            lualine_y = {},
            lualine_z = {}
        },
        tabline = {},
        winbar = {},
        inactive_winbar = {},
        extensions = {'fzf', 'fugitive', 'man'}
    })
