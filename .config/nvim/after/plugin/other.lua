local other = require("other-nvim")

other.setup({
    mappings = {
        {
            pattern = "(.+)/(.*).cpp$",
            target = {
                {
                    target = "%1/%2.h",
                    context = "header"
                },
                {
                    target = "%1/../include/%2.h",
                    context = "header"
                },
                {
                    target = "%1/../includes/%2.h",
                    context = "header"
                },
                {
                    target = "%1/../headers/%2.h",
                    context = "header"
                },
                {
                    target = "%1/%2.hpp",
                    context = "header"
                },
                {
                    target = "%1/../include/%2.hpp",
                    context = "header"
                },
                {
                    target = "%1/../includes/%2.hpp",
                    context = "header"
                },
                {
                    target = "%1/../headers/%2.hpp",
                    context = "header"
                },
            },
        },
        {
            pattern = "(.+)/(.*).hpp$",
            target = {
                {
                    target = "%1/%2.cpp",
                    context = "source"
                },
                {
                    target = "%1/../sources/%2.cpp",
                    context = "source"
                },
                {
                    target = "%1/../Sources/%2.cpp",
                    context = "source"
                },
            },
        },
        {
            pattern = "(.+)/(.*).h$",
            target = {
                {
                    target = "%1/%2.cpp",
                    context = "source"
                },
                {
                    target = "%1/../sources/%2.cpp",
                    context = "source"
                },
                {
                    target = "%1/../Sources/%2.cpp",
                    context = "source"
                },
                {
                    target = "%1/%2.c",
                    context = "source"
                },
                {
                    target = "%1/../sources/%2.c",
                    context = "source"
                },
                {
                    target = "%1/../Sources/%2.c",
                    context = "source"
                },
            },
        },
    },
})

