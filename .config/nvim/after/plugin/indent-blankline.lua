-- vim.cmd([[highlight IndentBlanklineIndent1 guifg=#888888 gui=nocombine]])
-- vim.cmd([[highlight IndentBlanklineIndent2 guifg=#666666 gui=nocombine]])

local rainbow_highlights = {
    "RainbowRed",
    "RainbowYellow",
    "RainbowBlue",
    "RainbowOrange",
    "RainbowGreen",
    "RainbowViolet",
    "RainbowCyan",
}
local hooks = require("ibl.hooks")

-- create the highlight groups in the highlight setup hook, so they are reset
-- every time the colorscheme changes
hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
    vim.api.nvim_set_hl(0, "RainbowRed", { fg = "#E06C75" })
    vim.api.nvim_set_hl(0, "RainbowYellow", { fg = "#E5C07B" })
    vim.api.nvim_set_hl(0, "RainbowBlue", { fg = "#61AFEF" })
    vim.api.nvim_set_hl(0, "RainbowOrange", { fg = "#D19A66" })
    vim.api.nvim_set_hl(0, "RainbowGreen", { fg = "#98C379" })
    vim.api.nvim_set_hl(0, "RainbowViolet", { fg = "#C678DD" })
    vim.api.nvim_set_hl(0, "RainbowCyan", { fg = "#56B6C2" })
end)

vim.g.rainbow_delimiters = { highlight = rainbow_highlights }

require("ibl").setup {
    scope = {
        enabled = true,
        show_exact_scope = false,
        highlight = rainbow_highlights,
    },
    whitespace = {
        remove_blankline_trail = false,
    },
    exclude = {
        buftypes = {"terminal", "nofile", "quickfix", "prompt", "gitdiff", "gitcommit"},
        filetypes = {"dashboard", "dart"}
    },
}

hooks.register(hooks.type.SCOPE_HIGHLIGHT, hooks.builtin.scope_highlight_from_extmark)
