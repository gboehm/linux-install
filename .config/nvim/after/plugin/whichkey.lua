function _G.scratch_popup()
    local popup = require("nui.popup")({
            enter = true,
            focusable = true,
            border = {
                style = "rounded",
                text = {
                    top = "Scratch buffer"
                },
            },
            position = "50%",
            size = {
                width = "80%",
                height = "60%",
            },
            win_options = {
                winhighlight = "Normal:Normal,FloatBorder:MoreMsg",
            }
        })

    -- mount/open the component
    popup:mount()

    local event = require("nui.utils.autocmd").event
    -- unmount component when cursor leaves buffer
    popup:on(event.BufLeave, function()
        popup:unmount()
    end, { once = true })
end

local wk = require('whichkey_setup')
wk.config{
    hide_statusline = false,
    default_keymap_settings = {
        silent = true,
        noremap = true,
    },
    default_mode = 'n',
}
local keymap = {
    ['<CR>'] = { ':noh<cr>', 'Hide search highlight' },
    b = {
        name='+buffers',
        b = { ':lua require("fzf-lua").buffers()<cr>', 'Search buffers' },
        d = { ':bd<cr>', 'Kill buffers' },
        D = { ':bufdo bd<cr>', 'Kill all buffers' },
        h = { ':new<cr>', 'New horizontal empty buffer' },
        l = { ':b#<cr>', 'Switch to last buffer' },
        O = { ':%bd|e#|bd#<cr>|\'"', 'Kill all other buffers' },
        s = { ':w!<cr>', 'Save buffer' },
        S = { ':bufdo w!<cr>', 'Save all buffers' },
        -- u = { ':SudoSave<cr>', 'Save buffer as root' },
        v = { ':vnew<cr>', 'New vertical empty buffer' },
        -- x = { '<cmd>lua _G.scratch_popup()<cr>', 'Pop up scratch buffer' }, -- TODO: Make it open the '/tmp/scratch' file ?
        -- X = { ':e /tmp/scratch<cr>', 'Switch to scratch buffer' }, -- WARN: Made functions
        y = { ':%y+<cr>', 'Yank buffer' },
    },
    c = {
        name='+code',
        a = { ':lua require("fzf-lua").lsp_code_actions()<cr>', 'LSP code actions on line' },
        c = {
            name='+compilation',
            d = { ':Dispatch! cmake -B build -D CMAKE_BUILD_TYPE=Debug -D CMAKE_EXPORT_COMPILE_COMMANDS=ON -S . && cp build/compile_commands.json .<CR>', 'Debug config' },
            r = { ':Dispatch! cmake -B build -D CMAKE_BUILD_TYPE=Release -D CMAKE_EXPORT_COMPILE_COMMANDS=ON -S . && cp build/compile_commands.json .<CR>', 'Release config' },
            R = { ':Dispatch! rm -rf ./build/<CR>', 'Remove build' },
            c = { ':Dispatch! cmake --build build<CR>', 'Compile' },
        },
        d = { ':lua vim.lsp.buf.definition()<cr>', 'Jump to definition' },
        D = { ':lua vim.lsp.buf.references()<cr>', 'Jump to references' },
        f = { ':lua vim.lsp.buf.format()<cr>', 'Format buffer' },
        g = { '<Plug>(doge-generate)', 'Generate documentation for the current line' },
        h = { ':lua vim.lsp.buf.hover()<cr>', 'Show info hover' },
        H = { ':lua vim.diagnostic.open_float()<cr>', 'Show diagnostic in float' },
        i = { ':lua vim.lsp.buf.implementation()<cr>', 'Lists implementations' },
        -- j = { ':lua vim.lsp.buf.document_symbol()<cr>', 'Jump to symbol in buffer' },
        j = { ':lua require("fzf-lua").lsp_document_symbols()<cr>', 'Jump to synbol in project' },
        J = { ':lua require("fzf-lua").lsp_workspace_symbols()<cr>', 'Jump to synbol in project' },
        l = {
            name='+language',
            f = {
                name='+flutter',
                t = { ':Telescope flutter commands<cr>', 'Flutter tools' },
            },
        },
        L = { ':Dispatch ./build/', 'Launch binary' },
        -- o = { 'call CocActionAsync("runCommand", "editor.action.organizeImport")', 'LSP Organize imports' },
        r = { ':lua vim.lsp.buf.rename()<cr>', 'LSP Rename' },
        w = { ':%s/\\s\\+$//e<cr>', 'Delete trailing whitespaces' },
        x = { ':lua require("fzf-lua").lsp_document_diagnostics()<cr>', 'List buffer diagnostic' },
        X = { ':lua require("fzf-lua").lsp_workspace_diagnostics()<cr>', 'List workspace diagnostic' },
    },
    d = {
        name='+debugger',
        b = {
            name='+breakpoints',
            b = { ':lua require"dap".toggle_breakpoint()<cr>', 'Toggle breakpoint' },
            l = { ':lua require"dap".list_breakpoints()<cr>', 'List breakpoints' },
            c = { ':lua require"dap".clear_breakpoints()<cr>', 'Clear breakpoints' },
        },
        c = { ':lua require"dap".continue()<cr>', 'Continue' },
        C = { ':lua require"dap".run_to_cursor()<cr>', 'Continue execution to the current cursor' },
        f = { ':lua require("fzf-lua").builtin()<cr>dap_', 'Fuzzy with dap' },
        u = {
            name='+ui',
            e = { ':lua require"dapui".eval()<cr>', 'Evaluate under cursor' },
            f = { ':lua require"dapui".float_element()<cr>', 'Float element' },
            t = { ':lua require"dapui".toggle()<cr>', 'Toggle UI' },
        },
        h = { ':lua require"dap.ui.widgets".hover()<cr>', 'Hover widgets' }, -- ??
        i = { ':lua require"dap".step_into()<cr>', 'Step into instruction' },
        l = { name='+language',
            l = {
                name='+lua',
                l = { ':lua require"osv".launch({port = 8086})<cr>', 'Launch debugger server' },
            },
            p = {
                name='+python',
                m = { ':lua require"dap-python".test_method()<cr>', 'Test method under cursor' },
                c = { ':lua require"dap-python".test_class()<cr>', 'Test class under cursor' },
                s = { ':lua require"dap-python".debug_selection()<cr>', 'Test current selection' },
            },
            r = {
                name='+rust',
                e = { ':RustExpand<cr>', 'Expands the current file' },
                p = { ':RustPlay<cr>', 'Sends selection or buffer to rust playpen' },
                r = { ':RustRun<cr>', 'Compile and run the current file' },
                f = { ':RustFmt<cr>', 'Format the current buffer' },
                F = { ':RustFmtRange<cr>', 'Format the current selection' },
            }
        },
        n = { ':lua require"dap".step_over()<cr>', 'Step over to next instruction' },
        o = { ':lua require"dap".step_out()<cr>', 'Step out of instruction' },
        p = { ':lua require"dap".step_back()<cr>', 'Step back to prev instruction' },
        P = { ':lua require"dap".reverse_continue()<cr>', 'Reverse execution until last bp' },
        r = { ':lua require"dap".restart()<cr>', 'Restart session' },
        s = {
            name='+stacktrace',
            u = { ':lua require"dap".up()<cr>', 'Go up the stack without stepping' },
            d = { ':lua require"dap".down()<cr>', 'Go down the stack without stepping' },
        },
        t = { ':lua require"dap".terminate()<cr>', 'Terminate' },
    },
    f = {
        name='+file',
        b = { ':BrootCurrentDir<cr>', 'Broot the current dir' },
        c = { ':lua require("fzf-lua").files({ cwd = "~/.config" })<cr>', 'Find file in config' },
        f = { ':lua require("fzf-lua").files()<cr>', 'Find file' },
        o = { ':Other<cr>' , 'Find alternative file' },
        p = { '1<C-g>' , 'Show current file path' },
        m  = {
            name = '+modify',
            r = { ':!mv %<Tab>', 'Rename/move file' },
            d = { ':call delete(@%)', 'Delete this file' },
        },
        s = { ':w!<cr>', 'Save file' },
        S = { ':w! ', 'Save file as...' },
        -- u = { '', 'Sudo find file' },
        -- U = { '', 'Sudo this file' },
        t = { ':Neotree toggle<cr>', 'Toggle Neotree' },
        y = { name='+yank',
            f = { ':let @" = expand("%:p")', 'Yank full file path' },
            F = { ':let @+ = expand("%:p")', '`f` to system clipboard' },
            l = { ':let @" = expand("%")', 'Yank file path relative to project' },
            L = { ':let @+ = expand("%")', '`l` to system clipboard' },
        },
        z = {
            name='+fuzzy',
            f = { ':lua require("fzf-lua").builtin()<cr>', 'FzfLua' },
        },
    },
    g = {
        name='+git',
        c = { ':Easypick conflicts<cr>',  'List conflicting files'},
        b = {
            name='+blame',
            b = { ':lua require("agitator").git_blame_toggle()<cr>', 'Toggle blame sidebar' },
            B = { ':lua require("agitator").search_git_branch()<cr>', 'Search for a file to blame' },
            t = { ':lua require("agitator").git_time_machine({use_current_win = true})<cr>', 'Open time machine for this file' },
        },
        d = { ':DiffviewOpen<cr>', 'Open a diff view for this file' },
        D = { ':DiffviewOpen ([oldRef...newRef]|[ref]) [-- file]', 'Open a diff view with parameters' },
        g = { ':Neogit<cr>', 'Open neogit' },
        h = { ':DiffviewFileHistory %<cr>', 'Open git history for this file' },
        H = { ':DiffviewFileHistory<cr>', 'Open git history for this branch' },
    },
    h = {
        name='+harpoon',
        a = { ':lua require("harpoon.mark").add_file()<cr>', 'Harpoon current file' },
        h = { ':lua require("harpoon.ui").toggle_quick_menu()<cr>', 'Open menu' },
        t = { ':Telescope harpoon marks<cr>', 'Telescope marks' },
        ['1'] = { ':lua require("harpoon.ui").nav_file(1)<cr>', 'Focus first mark' },
        ['2'] = { ':lua require("harpoon.ui").nav_file(2)<cr>', 'Focus second mark' },
        ['3'] = { ':lua require("harpoon.ui").nav_file(3)<cr>', 'Focus third mark' },
        ['4'] = { ':lua require("harpoon.ui").nav_file(4)<cr>', 'Focus forth mark' },
        ['5'] = { ':lua require("harpoon.ui").nav_file(5)<cr>', 'Focus fifth mark' },
        ['6'] = { ':lua require("harpoon.ui").nav_file(6)<cr>', 'Focus sixth mark' },
        ['7'] = { ':lua require("harpoon.ui").nav_file(7)<cr>', 'Focus seventh mark' },
        ['8'] = { ':lua require("harpoon.ui").nav_file(8)<cr>', 'Focus eigth mark' },
        ['9'] = { ':lua require("harpoon.ui").nav_file(9)<cr>', 'Focus nineth mark' },
        ['0'] = { ':lua require("harpoon.ui").nav_file(0)<cr>', 'Focus tenth mark' },
    },
    m = {
        name='+macros',
        m = { ':lua require("NeoComposer.ui").toggle_macro_menu()<cr>', 'Toggle macro menu' },
        p = { ':lua require("NeoComposer.ui").play_macro()<cr>', 'Play macro' },
        s = { ':lua require("NeoComposer.ui").stop_macro()<cr>', 'Play macro' },
        n = { ':lua require("NeoComposer.ui").cycle_next()<cr>', 'Cycle next macro' },
        N = { ':lua require("NeoComposer.ui").cycle_prev()<cr>', 'Cycle previous macro' },
        r = { ':lua require("NeoComposer.ui").toggle_record()<cr>', 'Toggle macro recording' },
    },
    n = {
        name='+sessions',
        d = { ':Autosession delete<cr>', 'Delete session' },
        f = { ':Autosession search<cr>', 'Find vim session' },
        s = { ':SessionSave<cr>', 'Save session' },
        l = { ':Telescope session-lens<cr>', 'Session lens' },
    },
    q = {
        name='+quickfix',
        v = { ':', 'Send selection to quickfix' },
        n = { ':cnext<cr>', 'Goto next in quickfix' },
        N = { ':cafter<cr>', 'Goto next in quickfix in buffer' },
        p = { ':cprev<cr>', 'Goto previous in quickfix' },
        P = { ':cbefore<cr>', 'Goto previous in quickfix in buffer' },
        q = { ':copen<cr>', 'Open quickfix list' },
        Q = { ':cclose<cr>', 'Close quickfix list' },
    },
    s = {
        name='+search',
        b = { ':lua require("fzf-lua").blines()<cr>', 'Search all open buffers' },
        B = { ':lua require("fzf-lua").lines()<cr>', 'Search all open buffers' },
        d = { ':lua require("fzf-lua").grep_project({ cwd = "%:p:h" })<cr>', 'Search current directory' },
        D = { ':lua require("fzf-lua").grep_project()<cr>', 'Search project directory' },
        t = {
            name='+tmux',
            b = { ':lua require("fzf-lua").tmux_buffers()<cr>', 'Search buffers ?' }, -- TODO: Test
            s = { ':Telescope tmux sessions<cr>', 'Search sessions' },
            w = { ':Telescope tmux windows<cr>', 'Search windows' },
        },
        v = { ':lua require("fzf-lua").grep_visual()<cr>', 'Search visual selection in project' },
        w = { ':lua require("fzf-lua").grep_cword()<cr>', 'Search word under cursor in project' },
        W = { ':lua require("fzf-lua").grep_cWORD()<cr>', 'Search WORD under cursor in project' },
        ['?'] = { ':TodoTelescope<cr>', 'Show all Todoish comments' }
    },
    t = {
        name='+toggle',
        c = { ':ColorToggle<cr>', 'Hex colors' },
        s = { ':setlocal spell!<cr>', 'Spell checker' },
        h = { ':HexokinaseToggle<cr>', 'Hexokinase' },
        i = { ':IBLToggle<cr>', 'Indent guides' },
        l = { ':set nu! rnu!<cr>', 'Line numbers' },
    },
    u = {
        name='+undo',
        u = { ':UndotreeToggle | UndotreeFocus<cr>', 'Toggle undotree' },
    },
    v = {
        name='+vim',
        c = { ':lua require("fzf-lua").commands()<cr>', 'Run vim command' },
        r = {
            name='+reload/restart',
            r = { ':luafile $MYVIMRC<cr>', 'Resource' },
        },
        p = {
            name='+packer',
            r = { ':PackerCompile<cr>:luafile $MYVIMRC<cr>', 'Compile' },
            s = { ':PackerSync<cr>', 'Sync' },
            i = { ':PackerInstall<cr>', 'Install' },
            c = { ':PackerClean<cr>', 'Clean' },
        },
    },
    w = {
        name='+window',
        d = { ':q<cr>', 'Kill window' },
        h = { '<C-W>h', 'Focus left window' },
        j = { '<C-W>j', 'Focus down window' },
        k = { '<C-W>k', 'Focus up window' },
        l = { '<C-W>l', 'Focus right window' },
        m = { ':lua require("maximize").toggle()<cr>', 'Toggle maximize current window' },
        t = {
            name='+tabs',
            ['1'] = { '1gt', 'Focus first tab' },
            ['2'] = { '2gt', 'Focus second tab' },
            ['3'] = { '3gt', 'Focus third tab' },
            ['4'] = { '4gt', 'Focus fourth tab' },
            ['5'] = { '5gt', 'Focus fifth tab' },
            ['6'] = { '6gt', 'Focus sixth tab' },
            ['7'] = { '7gt', 'Focus seventh tab' },
            ['8'] = { '8gt', 'Focus eighth tab' },
            ['9'] = { '9gt', 'Focus nineth tab' },
            ['0'] = { '0gt', 'Focus tenth tab' },
            d = { ':tabclose<cr>', 'Kill tab' },
            h = { 'gT', 'Focus left tab' },
            l = { 'gt', 'Focus right tab' },
            o = { ':tabedit %<cr>', 'Duplicate buffer in tab' },
            t = { ':tablast<cr>', 'Focus previously focused tab' },
        },
        w = { '<C-W>w', 'Focus floating window' },
    },
}
wk.register_keymap('leader', keymap)
wk.register_keymap('leader', keymap, { mode='v' })
