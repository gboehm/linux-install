local telescope = require("telescope")
local fb_actions = telescope.extensions.file_browser.actions

telescope.setup{
	defaults = {
		mappings = {
			i = {
				["C-j"] = "move_selection_next",
				["C-k"] = "move_selection_previous",
				["C-S"] = "select_horizontal",
				["C-V"] = "select_vertical",
			},
			n = {
				["<space>"] = "toggle_selection",
			}
		},
	path_display={ "smart" },
	},
	pickers = {
		buffers = {
			sort_lastused = true,
			ignore_current_buffer = true,
			attach_mappings = function(prompt_bufnr, map)
				local delete_buf = function()
					require('telescope.actions').delete_buffer(prompt_bufnr)
				end
				-- mode, key, func
				map('n', 'd', delete_buf)
				return true
			end
		},
		find_files = {
			follow = true,
		},
	},
	extensions = {
		file_browser = {
			hijack_netrw = false,
			cwd_to_path = true,
			mappings = {
				n = {
					["-"] = fb_actions.goto_parent_dir,
				},
			},
		},
		fzf = {
			fuzzy = true,
			override_generic_sorter = true,
			override_file_sorter =  true,
			case_mode = "smart_case",
		},
		frecency = {
			auto_validate = true,
			db_safe_mode = false,
			hide_current_buffer = true,
		},
	},
}

telescope.load_extension('file_browser')
telescope.load_extension('fzf')
telescope.load_extension('z')
telescope.load_extension('tmux')
telescope.load_extension('dap')
telescope.load_extension('ui-select')
telescope.load_extension('harpoon')
telescope.load_extension('session-lens')
telescope.load_extension('macros')
telescope.load_extension('undo')
telescope.load_extension('flutter')
