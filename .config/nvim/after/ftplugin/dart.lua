-- Kinda dumb workaround to use virtual_text when a dart file has been opened once
-- The only way to know if lsp_lines is enabled is to try and toggle them
if ToggleLspLines() then
    ToggleLspLines()
end

vim.opt.shiftwidth = 2
vim.opt.tabstop = 2
vim.opt.softtabstop = 2
