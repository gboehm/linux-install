local ensure_packer = function()
    local fn = vim.fn
    local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
        vim.cmd [[packadd packer.nvim]]
        return true
    end
    return false
end

local packer_bootstrap = ensure_packer()

return require('packer').startup(function(use)
    use 'wbthomason/packer.nvim'
    -- My plugins here

    use 'loctvl842/monokai-pro.nvim'

    -- Telescope
    use {
        "nvim-telescope/telescope.nvim",
        branch = '0.1.x',
        requires = {
            'nvim-lua/plenary.nvim',

            {
                'nvim-telescope/telescope-frecency.nvim',
                requires = {'kkharji/sqlite.lua'}
            },
            'nvim-telescope/telescope-file-browser.nvim',
            {
                'nvim-telescope/telescope-fzf-native.nvim',
                run = 'make'
            },
            'nvim-telescope/telescope-z.nvim',
            {
                'camgraff/telescope-tmux.nvim',
                requires = { 'norcalli/nvim-terminal.lua' }
            },
            {
                'nvim-telescope/telescope-dap.nvim',
                requires ={
                    'mfussenegger/nvim-dap',
                    'nvim-treesitter/nvim-treesitter',
                }
            },
            'nvim-telescope/telescope-ui-select.nvim',
            'axkirillov/easypick.nvim',
            'debugloop/telescope-undo.nvim',
        },
    }

    use {
        'ntpeters/vim-better-whitespace',
    }

    use {
        'ibhagwan/fzf-lua',
    }

    use {
        'nvim-lualine/lualine.nvim',
        requires = {
            "nvim-tree/nvim-web-devicons",
            "chrisbra/unicode.vim",
            "tpope/vim-characterize",
        }
    }

    use {
        "ecthelionvi/NeoComposer.nvim",
        requires = { "kkharji/sqlite.lua" },
    }

    use 'rmagatti/auto-session'

    use {
        "AckslD/nvim-whichkey-setup.lua",
        requires = {
            "liuchengxu/vim-which-key",
        },
    }

    use "tpope/vim-vinegar"              -- Netrw additions

    use "tpope/vim-surround"             -- Surrounding stuff is pretty cool
    use "tpope/vim-commentary"           -- Commenting stuff is pretty cool
    use {
        'kkoomen/vim-doge',
        run = ':call doge#install()'
    }

    use {
        "nvim-neo-tree/neo-tree.nvim",
        branch = "v3.x",
        requires = {
            "nvim-lua/plenary.nvim",
            "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
            "MunifTanjim/nui.nvim",
            "3rd/image.nvim", -- Optional image support in preview window: See `# Preview Mode` for more information
        }
    }
    use {
        'declancm/maximize.nvim',
    }

    use {
        "folke/todo-comments.nvim",
        required = "nvim-lua/plenary.nvim"
    }

    use "HiPhish/rainbow-delimiters.nvim"
    use {
        "lukas-reineke/indent-blankline.nvim",
        requires = "HiPhish/rainbow-delimiters.nvim",
        tag = 'v3.8.2', -- remove in a while the issue might be fixed
    }
    use {
        "wookayin/wilder.nvim",
        requires = {
            "sharkdp/fd",
            "nixprime/cpsm",
            "romgrk/fzy-lua-native",
            "nvim-tree/nvim-web-devicons",
        }
    }

    use {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v3.x',
        requires = {
            {'williamboman/mason.nvim'},
            {'williamboman/mason-lspconfig.nvim'},

            {'neovim/nvim-lspconfig'},
            {'hrsh7th/nvim-cmp'},
            {'hrsh7th/cmp-nvim-lsp'},
            {
                'L3MON4D3/LuaSnip',
                tag = "v2.*",
                run = "make install_jsregexp",
                requires = {
                    { "rafamadriz/friendly-snippets" },
                },
            },
            {'jiangmiao/auto-pairs'}, -- Some lsps seem to need it
        }
    }

    use 'ThePrimeagen/harpoon'

    use {
        "nvim-treesitter/nvim-treesitter",
        requires = {
            "nvim-treesitter/nvim-treesitter-context",
            "luckasRanarison/tree-sitter-hypr",
        }
    }

    use "tpope/vim-repeat" -- Supercharges `.`
    use "triglav/vim-visual-increment"

    use {
        "NeogitOrg/neogit",
        requires = {
            "chrisbra/unicode.vim",
            {
                "sindrets/diffview.nvim",
                requires = "nvim-lua/plenary.nvim"
            }
        }
    }
    use "lewis6991/gitsigns.nvim"
    use {
        "emmanueltouzery/agitator.nvim",
        requires = {
            'nvim-telescope/telescope.nvim',
            'nvim-lua/plenary.nvim'
        }
    }

    use {
        "iamcco/markdown-preview.nvim",
        requires = {
            "MunifTanjim/nui.nvim"
        }
    }

    use "rgroli/other.nvim"

    use "uga-rosa/ccc.nvim"

    use "Shatur/neovim-tasks" 	      -- Building tasks (eg. CMake), see `https://github.com/Shatur/neovim-tasks` for config info

    use {
        "mfussenegger/nvim-dap",
        requires = {
            'jbyuki/one-small-step-for-vimkind',
            'rcarriga/nvim-dap-ui',
            'nvim-neotest/nvim-nio',
            "mfussenegger/nvim-dap-python",
        },
    }

    use {
        'bennypowers/nvim-regexplainer',
        requires = {
            'nvim-treesitter/nvim-treesitter',
            'MunifTanjim/nui.nvim',
        },
    }

    use "mbbill/undotree"

    use {
        "kevinhwang91/nvim-bqf",
        requires = {
            'junegunn/fzf',
            run = function()
                vim.fn['fzf#install']()
            end
        }
    }

    use {
        'nvimdev/dashboard-nvim',
        event = 'VimEnter',
        requires = {'nvim-tree/nvim-web-devicons'},
    }

    use "ahmedkhalf/project.nvim"

    -- Languages support
    use "chr4/nginx.vim"
    use {
        'Fymyte/rasi.vim',
        ft = 'rasi',
    }
    use "m-pilia/vim-pkgbuild"
    use "lervag/vimtex"
    use {
        'akinsho/flutter-tools.nvim',
        requires = {
            'nvim-lua/plenary.nvim',
            'stevearc/dressing.nvim', -- optional for vim.ui.select
        },
    }

    -- WARN: ???
    use "mileszs/ack.vim" 		      -- Use GNU ack in vim
    use "mg979/vim-visual-multi" 	      -- Powerfull multi cursor editing

    -- Automatically set up your configuration after cloning packer.nvim
    -- Put this at the end after all plugins
    if packer_bootstrap then
        require('packer').sync()
    end
end)
