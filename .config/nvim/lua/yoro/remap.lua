vim.g.mapleader = " "

vim.keymap.set('i', 'kj', '<ESC>')

-- Copy to system clipboard
vim.keymap.set('v', '<C-c>', '"+y', { noremap = true })

vim.keymap.set('n', '<C-d>', '<C-d>zz')
vim.keymap.set('n', '<C-u>', '<C-u>zz')
vim.keymap.set('n', 'n', 'nzzzv')
vim.keymap.set('n', 'N', 'Nzzzv')

vim.keymap.set('v', '*', ':<C-u>call VisualSelection(\'\', \'\')<CR>/<C-R>=@/<CR><CR>', { noremap = true, silent = true })
vim.keymap.set('v', '#', ':<C-u>call VisualSelection(\'\', \'\')<CR>?<C-R>=@/<CR><CR>', { noremap = true, silent = true })

-- Set jumplist mark on xxj or xxk
vim.keymap.set('n', 'k', function()
    if tonumber(vim.v.count) > 1 then
        return "m'" .. vim.v.count .. 'k'
    else
        return 'k'
    end
end, {noremap = true, expr = true})
vim.keymap.set('n', 'j', function()
    if tonumber(vim.v.count) > 1 then
        return "m'" .. vim.v.count .. 'j'
    else
        return 'j'
    end
end, {noremap = true, expr = true})

-- Move between windows
vim.keymap.set('', '<C-j>', '<C-W>j')
vim.keymap.set('', '<C-k>', '<C-W>k')
vim.keymap.set('', '<C-h>', '<C-W>h')
vim.keymap.set('', '<C-l>', '<C-W>l')

-- Moving stuff
--- Move lines of text using Alt keys
vim.keymap.set('n', '<M-j>', 'mz:m+<cr>`z')
vim.keymap.set('n', '<M-k>', 'mz:m-2<cr>`z')
vim.keymap.set('v', '<M-j>', ':m\'>+<cr>`<my`>mzgv`yo`z')
vim.keymap.set('v', '<M-k>', ':m\'<-2<cr>`>my`<mzgv`yo`z')

-- No yank on paste
vim.keymap.set('v', 'gp', '"0p')

