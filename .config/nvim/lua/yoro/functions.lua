-- Show the current line diagnostic in a float
FloatDiagnostic = function() -- INFO: Not necessary but shows how it's done
    vim.diagnostic.open_float()
end
vim.api.nvim_create_user_command('FloatDiagnostic', 'lua FloatDiagnostic()', {})

-- Show LSP help about the symbol under the cursor in a float
vim.api.nvim_create_user_command('FloatSymbolHelp', 'lua vim.lsp.buf.hover()', {})

-- Popup scratch buffer
vim.api.nvim_create_user_command('ScratchPopup', 'lua _G.scratch_popup()', {})

-- Switch to scratch buffer
vim.api.nvim_create_user_command('ScratchBuffer', 'e /tmp/scratch', {})

-- Open LSP code actions through fzf-lua
vim.api.nvim_create_user_command('LspCodeActionsFzfLua', 'lua require("fzf-lua").lsp_code_actions()', {})

-- Toggle lsp_lines
ToggleLspLines = function()
    local lines = require("lsp_lines").toggle();
    vim.diagnostic.config({virtual_text = not lines})
    return lines
end
vim.api.nvim_create_user_command('ToggleLspLines', 'lua ToggleLspLines()', {})

-- Generate compile_commands.json with CMake in tmp folder and copies the result in the project root
vim.api.nvim_create_user_command('GenerateCompileCommandsCMake', 'silent exec "!rm -rf /tmp/nvim_compilecommands_build && cmake -B /tmp/nvim_compilecommands_build -D CMAKE_BUILD_TYPE=Debug -D CMAKE_EXPORT_COMPILE_COMMANDS=ON -S . && cp /tmp/nvim_compilecommands_build/compile_commands.json ."', {})

