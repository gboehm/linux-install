-- General stuf
vim.opt.encoding = 'utf8'
vim.opt.ffs = 'unix,dos,mac'

vim.opt.langmenu = "en"
vim.cmd('let $LANG=\'en\'')
vim.env.LANGUAGE = 'en_US.UTF-8'
vim.env.LC_ALL = 'en_US.UTF-8'
vim.opt.history = 500
vim.cmd('filetype plugin on')
vim.cmd('filetype indent on')
vim.opt.shell = '/usr/bin/zsh'

vim.opt.wildmenu = true
vim.opt.wildignore = '*.o,*~,*.pyc,*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store'

vim.opt.autoread = true -- Reload files changed from the outside
vim.cmd('au FocusGained,BufEnter * checktime')

vim.opt.magic = true -- Regexp

vim.opt.laststatus = 2 -- Always show status line
vim.opt.termguicolors = true
vim.cmd('set guicursor')

-- Formatting
vim.opt.expandtab = true
vim.opt.smarttab = true
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.g.signcolumn = 'yes' --Always show sighcolumn
vim.cmd('set ai "Auto indent')
vim.cmd('set si "Smart indent')
vim.cmd('set wrap "Wrap lines')

-- Use django syntax for .tpl files (CS)
vim.api.nvim_create_autocmd({"BufNewFile","BufRead"}, {
    pattern = {"*.tpl"},
    command = "set ft=htmldjango",
})

-- Delete trailing white spaces on save for the following types
vim.api.nvim_create_autocmd({"BufWritePre"}, {
    pattern = {"*.txt","*.js","*.py","*.wiki","*.sh","*.coffee"},
    command = "call CleanExtraSpaces()",
})

-- Files, backups
vim.opt.backup = false
vim.opt.wb = false
vim.opt.swapfile = false
vim.cmd([[
try
    set undodir=$XDG_DATA_HOME/nvim/temp_dirs/undodir
    set undofile
catch
endtry
]]) -- Persistent undo

-- File explorer
vim.g.netrw_keepdir = 0 -- Avoid weird copy errors
vim.g.netrw_localcopydircmd = 'cp -r' -- Copy recursively by default

-- Configure backspace
vim.cmd('set backspace=indent,eol,start')
vim.cmd('set whichwrap+=<,>,h,l')

vim.cmd('au BufReadPost * if line("\'\\"") > 1 && line("\'\\"") <= line("$") | exe "normal! g\'\\"" | endif') -- Return to last edit position when opening files

-- Search
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.hlsearch = true
vim.opt.incsearch = true
vim.g.MRU_Max_Entries = 400

-- Coloring
-- vim.cmd('colo yoro_colors')
vim.cmd('colo monokai-pro')
vim.cmd('syntax enable')
vim.opt.modeline = true
vim.opt.modelines = 5
vim.opt.background = "dark"

-- UI
vim.opt.ruler = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.so = 7 -- Set 7 lines to the cursor when moving vertically
vim.opt.cursorline = true
vim.opt.hid = true -- A buffer becomes hidden when it's abandonned
vim.opt.cmdheight = 1
vim.opt.showmode = false -- Hide --INSERT in the command line

-- Fixes
-- Suppress sounds
vim.opt.errorbells = false
vim.opt.visualbell = false
vim.opt.tm = 500
vim.g.t_vb = "" --FIXME Don't even know what that is

vim.g.snips_author = "Guillaume BOEHM"
vim.g.snips_email = "pro@mail.gboehm.com"
vim.g.snips_github = "https://github.com/guillaumeboehm"
