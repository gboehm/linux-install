#!/bin/sh

cwd="~"

if [ ! "$xrunning" ]; then
    # wayland env
    if [ "$HYPRLAND_CMD" ]; then
        #Hyprland
        cwd="$(hyprctl activewindow | grep -A1 -P "^\s*class: kitty$" | grep -Po "(?<=title: ).*$")"
    fi
else
    # X env
    HOSTNAME=$(cat /proc/sys/kernel/hostname)
    cwd="$(xprop -id "$(bspc query -N -n)" | grep -P "WM_NAME\(STRING\)" | grep -Po "(?<=\= \").*(?=\"$)")"
fi

cwd=$(echo "$cwd" | sed "s|^~|$HOME|")
if [ -d "$cwd" ]; then
    echo "cwd"
    kitty -d "$cwd" &
else
    echo "default"
    kitty -d "~" &
fi
