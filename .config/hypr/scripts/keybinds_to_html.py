import argparse

def parse_input_file(file_path):
    categories = []
    current_category = None
    current_section = None

    def remove_empty_category(categories):
        # Remove last category if no sections were added
        if len(categories) > 0:
            remove_empty_section(categories[-1]['sections'])
            if len(categories[-1]['sections']) == 0:
                categories.pop()

    def remove_empty_section(sections):
        # Remove last section if no shortcut were added
        if sections:
            if len(sections[-1]['shortcuts']) == 0:
                sections.pop()

    with open(file_path, 'r') as file:
        for line in file:
            line = line.strip()
            if line.startswith('#!'):

                remove_empty_category(categories)

                current_category = line[2:].strip()
                current_section = None
                categories.append({'title': current_category, 'sections': []})

            elif line.startswith('#??'):

                current_section = line[3:].strip()
                if categories:  # Check if categories list is not empty before accessing the last category
                    sections = categories[-1]['sections']

                    remove_empty_section(sections)

                    sections.append({'title': current_section, 'shortcuts': []})

            elif line.startswith('bind'):

                ignore=False
                shortcut = line.split('=', 1)[1].split(',', 2)

                if len(shortcut) > 1:

                    keys1, keys2, command = shortcut

                    main_keys=keys1.strip().split(' ')
                    sec_keys=keys2.strip().split(' ')

                    for key in main_keys+sec_keys:
                        if key.startswith('mouse'):
                            ignore=True

                    if not ignore and categories and current_category is not None and current_section is not None:  # Check if categories list is not empty and current_section is set

                        shortcut_item = {'main_keys': main_keys, 'sec_keys': sec_keys, 'command': command.strip()}

                        sections = categories[-1]['sections']
                        sections[-1]['shortcuts'].append(shortcut_item)

    return categories

def format_html(categories):
    html = '''\
<!DOCTYPE html>
<html>
    <head>
    </head>

    <style type="text/css">
        .wrapper {
          display: grid;
          grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
          grid-gap: 5px;
          grid-auto-flow: dense;
          list-style: none;
          padding: 0;
        }
        .wrapper li {
            width:100%;
            display: block;
        }
        * {
          background-color: #2d2a2e;
          color: #fcfcfa;
        }
        h2 {
            padding: 0;
            margin: 0;
            text-align:center;
            color: #ff6188;
        }
        .group {
            color: #fc9867;
            font-weight: bold;
            font-size: 1.2em;
            margin-bottom: -10px;
        }
        .bind {
            color: #7bd88f;
            font-style: italic;
        }
        .action {
            color: #40b2bf;
        }
        .command {
            color: #f0c674;
        }
        .icon {
            color: #ab9df2;
            position: absolute;
            display: inline-block;
            transform:translate(-15px, 0px);
            background: transparent;
        }
        span {
            margin: 0;
            padding: 0;
        }
        div {
            width: 100%;
            overflow-wrap: break-word;
        }
    </style>

    <body class="body">
        <h2>Hyprland Shortcuts</h2>
'''
    for category in categories:
        html += f'''\
        <span class='group'>### {category["title"]} ###</span>

        <ul class="wrapper">
'''
        for section in category['sections']:
            main_keys=set()
            sec_keys=set()
            for shortcut in section['shortcuts']:
                main_keys.add(' + '.join(shortcut['main_keys']))
                sec_keys.add(' + '.join(shortcut['sec_keys']))

            def get_keys_str(keys):
                if len(keys) == 0:
                    return ''
                elif len(keys) == 1:
                    return keys.pop()
                else:
                    return '{' + ', '.join(keys) + '}'

            keys_str=''
            if len(main_keys) > 0:
                keys_str += get_keys_str(main_keys)

            if len(sec_keys) > 0:
                if keys_str != '':
                    keys_str += ' + '
                keys_str += get_keys_str(sec_keys)

            html += f'''\
            <li><div>
                <span class='action'>- {section["title"]}</span>
                <br><span>&ensp;&ensp;&ensp;</span><span class='icon'></span>
                <span class='bind'>{keys_str}</span>
            </div></li>
'''

        html += f'</ul>'
    html += '</body></html>'

    return html


def generate_cheat_sheet(input_file, output_file):
    categories = parse_input_file(input_file)
    html = format_html(categories)

    with open(output_file, 'w') as file:
        file.write(html)

def main():
    parser = argparse.ArgumentParser(description='Generate an HTML cheat sheet from an input file.')
    parser.add_argument('-f', '--file', type=str, help='input file name')
    parser.add_argument('-o', '--output', type=str, help='output file name')
    args = parser.parse_args()

    if args.file and args.output:
        generate_cheat_sheet(args.file, args.output)
        print(f"Generated cheat sheet HTML: {args.output}")
    else:
        print("Please provide both input file and output file names.")

if __name__ == '__main__':
    main()
