import socket
import os

import gamescontrol

# e.g https://wiki.hyprland.org/IPC

sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
socket_path = "{}/hypr/{}/.socket2.sock".format(os.environ["XDG_RUNTIME_DIR"],os.environ["HYPRLAND_INSTANCE_SIGNATURE"])

try:
    # Connect to server and send data
    sock.connect(socket_path)

    print("Handling hypr events...")

    while True:
        data = sock.recv(1024)
        if not data:
            break
        line = data.decode("utf-8")
        handle = line.split(">>")
        action=handle[0]
        rules=handle[1].split(",")

        match action:
            case "openwindow":
                windowid = rules[0].strip()
                windowclass = "".join(rules[2:])

                gamescontrol.addGame(windowid, windowclass)

            case "closewindow":
                windowid = rules[0].strip()

                gamescontrol.removeGame(windowid)
finally:
    sock.close()
