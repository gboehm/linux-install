#!/bin/sh

width="$(hyprctl monitors -j | jq -r ".[] | select(.activeWorkspace.id == $(hyprctl activeworkspace -j | jq -r '.id')) | .width")"
height="$(hyprctl monitors -j | jq -r ".[] | select(.activeWorkspace.id == $(hyprctl activeworkspace -j | jq -r '.id')) | .height")"

yad --width="$(echo "($width * 0.9) / 1" | bc)" --height="$(echo "($height * 0.9) / 1" | bc)" --html --no-buttons --disable-search --borders=0 --sticky --on-top --skip-taskbar < /tmp/keybinds.html
