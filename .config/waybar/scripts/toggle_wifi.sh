#!/bin/sh

if [ "$(nmcli radio wifi)" = "enabled" ]; then
    echo "disabling wifi"
    nmcli radio wifi off
    echo "wifi off"
else
    echo "enabling wifi"
    nmcli radio wifi on
    echo "wifi on"
fi
