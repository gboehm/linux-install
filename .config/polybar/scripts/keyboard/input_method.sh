#!/bin/sh

# im=$(gdbus call -e -d org.fcitx.Fcitx -o "/inputmethod" -m "org.fcitx.Fcitx.InputMethod.GetCurrentIM" | sed -nr "s/(\(')|(',.*)//gp")
im=$(qdbus "org.fcitx.Fcitx5" "/controller" "org.fcitx.Fcitx.Controller1.CurrentInputMethod")
# echo $im

# bit of hacking to display all input methods including japanese
if [ -z "$im" ]; then echo "en"; exit; fi

case $im in
    mozc)
        im="jp"
        ;;
    *-fr)
        im="fr"
        ;;
    *-us)
        im="en"
        ;;
    *-rus)
        im="ru"
        ;;
esac

echo "$im"
