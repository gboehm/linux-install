#!/bin/sh

question=$(echo " bsp -r|| lock| logout| reboot| shutdown" | rofi -config ~/.config/rofi/config-sysmenu.rasi -sep "|" -dmenu -i -eh 1)

case $question in
    *bsp_reload)
        bspc wm -r
        ;;
    *lock)
        dm-tool lock
        ;;
    *logout)
        #command -v gnome-session-quit 2>/dev/null 2>&1 || command -v session-logout >/dev/null 2>&1
        # command -v session-logout
        loginctl terminate-user $USER
        ;;
    *reboot)
        systemctl reboot
        ;;
    *shutdown)
        systemctl poweroff
        ;;
    *)
        exit 0  # do nothing on wrong response
        ;;
esac

