#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use
# polybar-msg cmd quit

# load different colors if pywal is running on the system or not
if command -v wal &> /dev/null
then
    colors_script="$HOME/.cache/wal/colors-polybar.sh"
else
    colors_script="$HOME/.config/polybar/default_colors.sh"
fi

# shellcheck source=/home/yoro/.cache/wal/colors-polybar.sh
. "$colors_script"

while IFS='' read -r line
do
    export "${line//\'}"
done < "$colors_script"
export wal_bg_alpha="#cc${background/'#'}"
DEFAULT_WLAN_INTERFACE="$( ip link show | grep -oP "(?<=[0-9]: )wl[^ \s:]+" )"
export DEFAULT_WLAN_INTERFACE
DEFAULT_WIRED_INTERFACE="$( ip link show | grep -oP "(?<=[0-9]: )en[^ \s:]+" )"
export DEFAULT_WIRED_INTERFACE

main_bar=main
sec_bar=sec

if type "xrandr"; then
    main_screen=$(xrandr --query | grep " connected primary" | cut -d" " -f1)
    sec_screens=$(xrandr --query | grep -P " connected(?!\s*primary)" | cut -d" " -f1)

    echo "---" | tee -a "/tmp/polybar_${main_screen}.log"
    MONITOR=$main_screen polybar $main_bar -c "$HOME/.config/polybar/${main_bar}/config.ini" >> "/tmp/polybar_${main_screen}.log" 2>&1 & disown
    for screen in $sec_screens; do
        echo "---" | tee -a "/tmp/polybar_${screen}.log"
        MONITOR=$screen polybar $sec_bar -c "$HOME/.config/polybar/${sec_bar}/config.ini" >> "/tmp/polybar_${screen}.log" 2>&1 & disown
    done
else
    # Launch default bar
    echo "---" | tee -a "/tmp/polybar_${main_bar}.log"
    polybar $main_bar -c "$HOME/.config/polybar/${main_bar}/config.ini" >> "/tmp/polybar_${main_bar}.log" 2>&1 & disown
fi

echo "Bars launched..."
