## To Do IMPORTANT

- Implement a CLI for mControlCenter
    * [x] POC
    * [ ] full impl
- implement gradual shift on rust hyprshade rework

## To Do

- nvim sessions config rework
    * [ ] rename sessions
    * [ ] quick jump between sessions
    * [ ] save session on jump to other session with space-S-f
- waybar custom-recorder filepicker
- hyprland
    * [ ] better first open window locations
- simple tab groups bugs
    * [ ] check with vimium weird tab switch going on a different group sometimes
    * [ ] super laggy with a lot of groups

## To Do LONG

- make issue for jshelter
    > JShelter's storage issue makes it impossible to add another page to exceptions and such
    * [ ] Find a fix
    * [ ] Implement the fix
    * [ ] Make PR https://pagure.io/JShelter/webextension/issues
