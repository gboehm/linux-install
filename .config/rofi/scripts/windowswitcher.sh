#!/bin/sh
# Switch focus to the selected window

# TODO: Change
CONFIG="$HOME/.config/rofi/config-windowswitcher.rasi"

if [ -z "$WAYLAND_DISPLAY" ]; then
    #X11

    # Do nothing
    true

else
    # Wayland

    if ! eval "command -v hyprctl &> /dev/null"; then
        notify-send "Error: hyprctl not found."
        exit 1
    fi

    window=$(hyprctl clients -j | jq -r ".[] | select(.class != \"\") | .class,.title,.address" | sed -E "s/org\.(.*)\.desktop/\1/" | gawk "
BEGIN{current_column=0; columns=3}
{
    current_column = current_column + 1
    if(current_column == 1) {
        printf \$0\" : \"
    }
    else if(current_column == 2) {
        printf \$0
    }
    else {
        current_column = 0
        print \" (\"\$0\")\"
    }
}" | rofi -dmenu -p 'Window:' -i -config "$CONFIG" | grep -Po '(?<=\()0x[0-9a-z]+(?=\))');

    hyprctl dispatch focuswindow address:"$window"
fi
