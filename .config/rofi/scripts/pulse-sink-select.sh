#!/bin/sh
# changes default sink and moves all streams to that sink

CONFIG="$HOME/.config/rofi/config-sinkselector.rasi"

# Blacklisted sinks "|" separated
ignoredSinks="GP104|Navi|<SomeOtherStuff>"

if [ -z "$WAYLAND_DISPLAY" ]; then
    #X11

    if ! eval "command -v ponymix &> /dev/null"; then
        notify-send "Error: Install ponymix."
        exit 1
    fi

    currentSink="$(pacmd info | grep -Po "(?<=$(pacmd info | grep -Po "(?<=Default sink name: ).*")/#\d: ).*")"
    sink=$(ponymix -t sink list | awk '/^sink/ {s=$1" "$2;getline;gsub(/^ +/,"",$0);print s" "$0}' | grep -vE "$ignoredSinks" | grep -v "$currentSink" | rofi -dmenu -p 'pulseaudio sink:' -i -config "$CONFIG" | grep -Po '[0-9]+(?=:)') &&

    ponymix set-default -d "$sink" &&
    for input in $(ponymix list -t sink-input | grep -Po '[0-9]+(?=:)'); do
        echo "$input -> $sink"
        ponymix -t sink-input -d "$input" move "$sink"
    done

else
    # Wayland

    if ! eval "command -v wpctl &> /dev/null"; then
        notify-send "Error: install wireplumber."
        exit 1
    fi

    sink=$(wpctl status | gawk "
{
    if (\$1 == \"Audio\"){
        audio = 1
    }

    if(audio) {
        if (\$2\$3 == \"Sources:\"){
            afterSinks = 1
        }

        if(sinks && ! afterSinks && \$2 && \$2 != \"*\"){
            sink=\"\"
            for (i=3; i<=(NF - 2); i++){
                sink = sink \" \" \$(i)
            }
            if(\$3 !~ \"$ignoredSinks\"){
                print \$2 sink
            }
        }

        if (\$2 == \"Sinks:\"){
            sinks = 1
        }
    }
}" | rofi -dmenu -p 'pulseaudio sink:' -i -config "$CONFIG" | grep -Po '[0-9]+(?=\.)');

    wpctl set-default "$sink"

fi
