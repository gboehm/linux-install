#!/bin/sh

question=$(echo "󰑥  reload||󰍁  lock|󰀍  logout|󰑐  reboot|󰐥  shutdown" | rofi -config ~/.config/rofi/config-sysmenu.rasi -sep "|" -dmenu -i -eh 1)

xsession=$(pidof bspwm)

case $question in
    *bsp_reload)
        if test "$xsession"; then
            bspc wm -r
        else
            hyprctl reload
        fi
        ;;
    *lock)
        if test "$xsession"; then
            dm-tool lock
        else
            hyprlock
        fi
        ;;
    *logout)
        if test "$xsession"; then
            bspc quit
        else
            hyprctl dispatch exit
        fi
        ;;
    *reboot)
        systemctl reboot
        ;;
    *shutdown)
        systemctl poweroff
        ;;
    *)
        exit 0  # do nothing on wrong response
        ;;
esac

