#!/bin/bash

# the cwd needs to be where the applet is
qmk compile && cd /usr/bin/ && mdloader --first --download ~/.config/qmk/firmware/massdrop_alt_guillaumeboehm.bin --restart
