import hid

vid = 0x04D8	# Change it for your device
pid = 0xEED3	# Change it for your device
usage_page = 0xFF60
usage = 0x61
pack_size = 32

buf = bytearray(pack_size)

with hid.Device(vid, pid) as h:
    print(f'Device manufacturer: {h.manufacturer}')
    print(f'Product: {h.product}')
    print(f'Serial Number: {h.serial}')
    print(h.read(8))
    h.nonblocking = 1

    # print(bytes( bytearray(8) ))
    print("Sending Data")
    # h.write(buf.hex())
    h.write(bytes(bytearray(32)))
    print("Receiving Data")
    print(h.read(pack_size))
