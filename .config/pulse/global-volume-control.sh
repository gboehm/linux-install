#!/bin/bash

sinks=$( pulsemixer --list-sinks | grep -Poh '(?<=ID: sink-)[0-9]+' )
action=$1

for sink in $sinks;
do
    case $action in
        mute)
            pulsemixer --id $sink --toggle-mute
            ;;
        voldown)
            pulsemixer --id $sink --change-volume -5
            ;;
        volup)
            pulsemixer --id $sink --change-volume +5
            ;;
        reset)
            pulsemixer --id $sink --set-volume 60
            ;;
    esac
done
