#!/bin/sh

#
# Displays some system info on the last desktop of the primary monitor (by default)
#

monitor_count=$(xrandr | grep -cP " connected(?!( primary)? \()")
current_layout="$1"
position=""

primary=''
if [ "$monitor_count" = 1 ]; then
    primary=$(xrandr | grep -P " connected")
else
    primary=$(xrandr | grep -P " connected primary")
fi

set_position(){
    if [ "$monitor_count" -ge "$1" ]; then
        position="$(bspc query -D -m "$(eval "$HOME/.config/bspwm/scripts/monitor_order.sh" | awk "NR==$1 {print}")" | tail -1 )"
    else
        position=$(bspc query -D -m "$(echo "$primary" | awk '{print $1}')" | tail -1 )
    fi
}

case "$current_layout" in
    "laptop_on_desk")
        set_position 3
        ;;
    *)
        case $monitor_count in
            *)
                position=$(bspc query -D -m "$(echo "$primary" | awk '{print $1}')" | tail -1 )
                ;;
        esac
        ;;
esac

size_calc(){ # size_calc monitor_size window_size_perc
    monitor_size=$1
    window_size_perc=$2
    python -c "print('%d' % ($monitor_size*$window_size_perc))"
}
offset_calc(){ # offset_calc monitor_offset monitor_size window_offset_perc
    monitor_offset=$1
    monitor_size=$2
    window_offset_perc=$3
    python -c "print('%d' % ($monitor_offset+($monitor_size*$window_offset_perc)))"
}

size_pos=$(echo "$primary" | grep -oP '\d*x\d*\+\d*\+\d*')
width=$(echo "$size_pos" | awk -F 'x' '{print $1}')
height=$(echo "$size_pos" | awk -F 'x' '{print $2}' | awk -F '+' '{print $1}')
xOffset=$(echo "$size_pos" | awk -F 'x' '{print $2}' | awk -F '+' '{print $2}')
yOffset=$(echo "$size_pos" | awk -F 'x' '{print $2}' | awk -F '+' '{print $3}')

if [ "$(pgrep -cf "kitty --class=Kitty_neofetch")" -eq 0 ]; then
    w_width=0.453125
    w_height=0.41
    w_xOffset=0.5375
    w_yOffset=0.565
    bspc rule -a Kitty_neofetch -o state=floating desktop="$position" rectangle="$(size_calc "$width" "$w_width")x$(size_calc "$height" "$w_height")+$(offset_calc "$xOffset" "$width" "$w_xOffset")+$(offset_calc "$yOffset" "$height" "$w_yOffset")"
    sleep 0.1
    kitty --class=Kitty_neofetch -o allow_remote_control=yes -o enabled_layouts=tall -c ~/.config/neofetch/kitty.conf /bin/zsh -c 'neofetch; kitty @ scroll-window 10p-; sleep infinity' &
fi
if [ "$(pgrep -cf "kitty --class=Kitty_btop")" -eq 0 ]; then
    w_width=0.5203125
    w_height=0.719444
    w_xOffset=0.00520833
    w_yOffset=0.09074
    bspc rule -a Kitty_btop -o state=floating desktop="$position" rectangle="$(size_calc "$width" "$w_width")x$(size_calc "$height" "$w_height")+$(offset_calc "$xOffset" "$width" "$w_xOffset")+$(offset_calc "$yOffset" "$height" "$w_yOffset")"
    sleep 0.1
    kitty --class=Kitty_btop btop &
fi
if [ "$(pgrep -cf "kitty --class=Kitty_nvtop")" -eq 0 ]; then
    w_width=0.3651041
    w_height=0.47407407
    w_xOffset=0.5640625
    w_yOffset=0.06018518
    bspc rule -a Kitty_nvtop -o state=floating desktop="$position" rectangle="$(size_calc "$width" "$w_width")x$(size_calc "$height" "$w_height")+$(offset_calc "$xOffset" "$width" "$w_xOffset")+$(offset_calc "$yOffset" "$height" "$w_yOffset")"
    sleep 0.1
    kitty --class=Kitty_nvtop nvtop &
fi
