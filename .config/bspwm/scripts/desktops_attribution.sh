#!/bin/bash
# Args : primary_monitor_desktop_count other_monitors_desktop_count

desktop_markers='一 二 三 四 五 六 七 八 九 十'
monitor_list=($(~/.config/bspwm/scripts/monitor_order.sh))
monitor_count=${#monitor_list[@]}

IFS=' ' read -r -a desktop_markers <<< "$desktop_markers"
if [ $# -eq 2 ]; then
    desktops_for_primary_monitor=$1
    desktops_for_other_monitors=$2
else
    desktops_for_primary_monitor=$(python -c "import math; print(math.ceil(${#desktop_markers[@]}/$monitor_count))")
    desktops_for_other_monitors=$(python -c "import math; print(math.floor(${#desktop_markers[@]}/$monitor_count))")
fi

primary_monitor=$(xrandr | grep -P " connected primary" | awk '{print $1}')

offset=0
for monitor in ${monitor_list[@]}
do
    if [ $monitor == $primary_monitor ]; then
        desktops_count=$desktops_for_primary_monitor
    else
        desktops_count=$desktops_for_other_monitors
    fi

    bspc monitor $monitor -d ${desktop_markers[@]:$offset:$desktops_count}
    offset=$(($offset+$desktops_count))
done
