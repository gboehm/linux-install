#!/bin/sh

# Returns a return separated list of MonitorName1 DeviceName1 Monitor2 Device2...

displays=$( ./monitor_order.sh )

prop_lines=$(xrandr --prop)

edid=""
oof=false

skipped=false
for display in $displays; do
    if [ ! $skipped ]; then
        skipped=true
    else
        xrandr --prop | sed -E "s/^$//"
    fi
    prev_display=$display
done


for line in $prop_lines; do
    if $( echo "$displays" | grep -qx "$( echo "$line" | sed 's/^-//' )" ); then
        oof=true
        display="$line"
    fi
    if $oof; then
            date
        if $( echo "$line" | sed 's/^-//' | grep -qx "GAMMA_LUT_SIZE:" ); then
            oof=false;
            echo "$edid" | xxd -r -p | parse-edid 2> /dev/null | grep "Identifier" | sed -E 's/^[[:blank:]]+Identifier \"(.*)\"$/\1/'
            echo "$display"
            edid=""
        else
            edid="$edid"$(echo "$line" | grep -P "^[[:xdigit:]]{32}$")
        fi
    fi
done
