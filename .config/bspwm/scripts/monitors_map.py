import subprocess
import re
import pyedid

xrandr_props = subprocess.check_output(["xrandr","--prop"]).decode(("utf-8"))

theregextorulethemall = re.compile(r"((?P<name>^[\w-]+) connected.*$\s+EDID:\s+(?P<edid>(?:[a-f0-9]{32}$\s+)+))",re.MULTILINE)

matches = theregextorulethemall.findall(xrandr_props)

for match in matches:
    name = match[1]
    edid = pyedid.parse_edid(match[2])
    print(edid.name,name,";")
