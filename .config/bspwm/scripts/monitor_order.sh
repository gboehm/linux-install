#!/bin/sh

xrandr -q | grep -P " connected(?!( primary)? \()" | sed -E "s/([^\ ]+)(.*[0-9]+x[0-9]+\+)([0-9]+)(.*)/\3 \1/g" | sort | awk '{print $2}'
