#!/bin/sh

size=$(xrandr --current | grep $(bspc query -M -m --names) | grep -P '\d+x\d+' -o | tr 'x' ' ')
side='both'
mult='none'
if test $# -gt 0; then
    side=$1
fi
if test $# -gt 1; then
    mult=$2
fi
set -- $size

if test "$side" != "both"; then
    if test "$side" = 'width'; then
        size=$1
    elif test "$side" = 'height'; then
        size=$2
    fi
fi
if test "$mult" != "none"; then
    IFS='.'
    set -- $(echo "$size * $mult" | bc -l)
    size=$1
fi
echo $size
