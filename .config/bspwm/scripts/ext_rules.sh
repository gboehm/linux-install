#!/bin/bash

wid=$1
class=$2
instance=$3
consequences=$4
title=$(xtitle "$wid")
echo -ne "id \"$wid\"\nclass \"$class\"\ninst \"$instance\"\nconsequences \"$consequences\"\ntitle \"$title\"\n\n" >> /tmp/bsp_extrules.log

main() {
    #### Class
	case "$class" in
		# firefox)
            # window_role=$(xprop -id $wid | grep "WM_WINDOW_ROLE" | grep -Po '"\K[^,"]+')
            # name=$(xprop -id $wid | grep "WM_NAME" | grep -Po '"\K[^,"]+')
            # user_time=$(xprop -id $wid | grep "WM_USER_TIME" | grep -Po '"\K[^,"]+')
            # echo -ne "firefox >> wr='$window_role' name='$name' user_time='$user_time'" >> /tmp/bsp_extrules.log
		# 	if [ "$instance" = "Toolkit" ];
            # then
                # # For firefox Toolkits
		# 		echo "state=floating sticky=on"
            # elif [ "$window_role" = "Dialog" ] || [ "$window_role" = "GtkFileChooserDialog" ] || test -z $user_time;
            # then
                # # For popups from firefox
                # echo "state=floating"
            # elif test -z $(expr "$name" : "^Mozilla Firefox");
            # then
                # # For windows opened from firefox
                # echo "monitor=^4 follow=off focus=off"
            # fi
		# 	;;
		Spotify)
			echo "monitor=^4 follow=off focus=off"
			;;
		"")
            sleep 0.5

			wm_class=$(xprop -id "$wid" | grep "WM_CLASS" | grep -Po '"\K[^,"]+')
            echo "$wm_class" >> /tmp/bsp_extrules.log

            class=$(echo "$wm_class" | awk -F ' ' "{print \$NF}")
            echo "$class" >> /tmp/bsp_extrules.log

            [ "$(echo "$wm_class" | wc -l)" = "2" ] && class=$(echo "$wm_class" | awk -F  ' ' "{print \$1}")
            echo "$class" >> /tmp/bsp_extrules.log

			test -n "$class" && main
			;;
	esac
    ##### Title
	case "$title" in
		Emulator)
            echo "state=floating"
			;;
	esac
}

main
